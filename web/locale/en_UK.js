/**
 * Locale en_UK
 */
var _l = {

	/******************************
	 * Cala
	 */
	cala_online: 'Online/Global',
	cala_allCountries: 'Online / Everywhere',
	cala_other: 'Other',
	cala_working: 'Working on it...',
	cala_alert: 'Alert!',
	cala_brilliant: 'Brilliant ;)',
	cala_err: 'Err!',
	cala_warning: 'Warning!',
	cala_userPwdSent: 'Please check your email for your new password. <br /> Remember to look in your <strong>JUNK</strong> mail just in case.',
	cala_userPwdSentErr: 'There was an error, please check your email address or username.',
	users_regUserExists: 'This user name or email is already taken. Please select a different one.',
	users_regFillAll: 'There was a problem, please check all required fields and try again.',

	/******************************
	 * LeStatz
	 */
	ls_ref: 'Ref',
	ls_goal: 'Goal',
	ls_visitor: 'Visitor',
	ls_pageLang: 'Page language',
	ls_browserLang: 'Browser language',
	ls_browser: 'Browser',
	ls_os: 'OS',
	ls_location: 'Location',
	ls_ip: 'IP',
	ls_postalCode: 'Postal Code',
	ls_coordinates: 'Coordinates',
	
	
	
	
	ls_logDown: 'Download/Save Log',
	ls_liveStats: 'Live Stats',
	ls_reports: 'View Reports',
	ls_briefReport: 'Overview',
	ls_visitorsLive: 'Visitors in the last minute',
	ls_visitsTotal: 'Page Views',
	ls_visitsUnique: 'Unique Visitors',
	ls_visitsRecent: 'Recent Visits',
	ls_currentLive: 'Current Live',
	ls_visitors: 'Visitors',
	ls_lastVisit: 'Last visit was',
	ls_refs: 'Refs',
	ls_total: 'Total',
	ls_totalP: 'Total %',
	ls_unique: 'Unique',
	ls_uniqueP: 'Unique %',
	ls_goals: 'Goals',
	ls_otherDets: 'Other details',
	ls_pagesUnique: 'Unique pages',
	ls_browsers: 'Browsers',
	ls_countries: 'Countries',
	ls_oss: 'Operating Systems',
	ls_config: 'Configure',
	ls_newDomainCreate: 'Add new domain',
	ls_codeGetHelp: 'You may get the domain`s script code in the "Configure" section.',
	lestatz_domainBasicInfo: 'Basic information',
	lestatz_url: 'Domain url or app name',
	lestatz_urlHelp: 'This is the main url for your domain. If you have subdomains you may use the same code for all or you may create new ones as you see fit. If you will use this code for an app you may just put the name of the app here.',
	lestatz_name: 'Name',
	lestatz_nameHelp: 'A unique name for this domain',
	lestatz_tokenSecret: 'Secret',
	lestatz_tokenSecretHelp: 'If you will provide access to your users you need a secret token. For more information about this please refer to the help section.',
	lestatz_timezone: 'Timezone',
	lestatz_timezoneHelp: 'You may use a different timezone for this domain',
	lestatz_token: 'Token',
	lestatz_tokenHelp: 'This is a unique code for your site, you may need it.',
	lestatz_addDomainButton: 'Add Domain',
	lestatz_editDomainButton: 'Edit Domain',
	lestatz_domainCode: 'Codes',
	lestatz_domainCodeH: 'You may use this code in your site or app.',

	ls_quickLog: 'Preset Reports',
	ls_quickLogLeg: 'You may use this presets, be careful as some of them may take some time to load and even freeze your browser.',
	ls_lastHour: 'Last hour',
	ls_lastSixHours: 'Last six hours',
	ls_lastTwelveHours: 'Last 12 hours',
	ls_today: 'Today',
	ls_thisWeek: 'This week',
	ls_twoWeeks: 'Last two weeks',
	ls_lastMoth: 'Last month',
	ls_logGen: 'Get log',
	ls_datesLog: 'By dates',
	ls_datesLogLeg: 'You can be more specific with this option but still be careful, it can be quite a big log and freeze your browser.',
	ls_startDate: 'Start date',
	ls_endDate: 'End date',
	ls_logLoad: 'Load from file',
	ls_logLoadLeg: 'If you have a saved log in csv form you may load it here. Again, be careful, this can freeze your browser for a while.',

};
