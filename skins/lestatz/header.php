<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
"http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Security-Policy" content="default-src * 'self' 'unsafe-inline' 'unsafe-eval';">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui, viewport-fit=cover">

		<!-- No Cache -->
		<meta http-equiv="Cache-Control" content="no-store" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="Expires" content="-1" />
		<meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />
		<!-- No Cache -->

		<meta name="description" content="<?php print $skin['header']['description'] ?>">
		<meta property="og:image" content="<?php print $skin['header']['image'] ?>">
		<link rel="shortcut icon" href="<?= $skin['siteUrl'] ?>web/img/favico.png"/>
		<title><?php print $skin['title'] ?></title>

		<!-- Css -->
		<link rel="stylesheet" href="<?= $skin['siteUrl'] ?>web/css/pure-min.css" />
		<link rel="stylesheet" href="<?= $skin['siteUrl'] ?>web/css/pure-grids-responsive-min.css" />
		<link rel="stylesheet" href="<?= $skin['siteUrl'] ?>web/css/iconmoon/style.css" />
		<link rel="stylesheet" href="web/swal/borderless.css" />
		<link rel="stylesheet" href="<?= $skin['siteUrl'] ?>web/css/cala.css" />

		<?php print $skin['fileCssList'] ?>

		<!-- Scripts -->
		<?php print $skin['header']['fileScripts'] ?>

		<script src="<?= $skin['siteUrl'] ?>web/locale/<?= $skin['locale'] ?>.js"></script>
		<script src="<?= $skin['siteUrl'] ?>web/jquery.js"></script>
		<script src="<?= $skin['siteUrl'] ?>web/cala.js"></script>

		<!-- More Css -->
		<?php print $skin['textCss'] ?>

	</head>
	<body>

		<div id="navbar">
			<!-- Top menu -->
			<div id="topMenu" align="left" style="padding: 5px;">
				<table width="100%" id="topMenuTable">
					<tr>
						<td valign="middle" id="navbarLeft" width="" align="left">
							<a href="<?= $skin['siteUrl'] ?>">
								<img src="web/img/webBanner_big.png" style="height: 30px;" class="notVisibleSmall" />
								<img src="web/img/webBanner_small.png" style="height: 30px;" class="notVisibleBig" />
							</a>
						</td>
						<td align="right" style="padding-right: 5px;" valign="middle">
							<span class="pointer notVisibleBig" style="font-size: 1.5rem; color: #fff;" onClick="">
								<span class="icon-menu"></span>
							</span>
						</td>
				</table>
			</div>
			<!-- //Top menu -->

		</div>

		<!-- Notifications go here -->
		<div id="notificationsContainer">
			<div id="notifications"></div>
		</div>
		<!-- //Notifications go here -->

		<!-- Top Messages -->
		<div align="center">
			<div style="position: absolute;">
				<div id="loader_global" align="center" style="position: relative; z-index: 10;"></div>
			</div>

			<div align="left">
				<!-- Info messages-->
				<?= $skin['messages']['info'] ?>
				<!-- //Info messages-->

				<!-- Warning messages-->
				<?= $skin['messages']['warning'] ?>
				<!-- //Warning messages-->

				<!-- Error messages-->
				<?= $skin['messages']['err'] ?>
				<!-- //Error messages-->
			</div>
		</div>
		<!-- //Top Messages -->


