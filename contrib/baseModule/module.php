<?php
/** @file module.php
 * A brief file description.
 * A more elaborated file description.
 */

/** \addtogroup Contrib
 *  @{
 */

/**
 * \defgroup ModuleName
 * @{
 */


/**
 * Boot up procedure
 */
function MODULE_NAME_bootMeUp()
{
    // Booting up routines that you require
}

/**
 * Init function/hook.
 */
function MODULE_NAME_init()
{

    // A list of paths that your module will use
    // they are done like this: http://yoursite/?w=baseModule_say_hello&param1=val
    $paths = array(
        array(

            // Request parameter like 'MODULE_NAME_blog_number'
            // They ALLWAYS start with 'MODULE_NAME_' because that is how
            // MyCala knows to look for your module

            'r' => 'baseModule_say_hello',
            // The action to perform, aka the function I will call
            // I will pass the params (see bellow) to this function
            'action' => 'baseModule_sayHello',

            // Access function to call and check for this request
            // You may define your own or use the ones provided by MyCala
            // users_openAccess lets ANYONE access this request
            // users_isLoggedIn checks if the request is done by a logged in
            // user, in this case extra params 'iam' and 'token' are required,
            // but you do not need to indicate them here, that is automatic
            'access' => 'users_openAccess',
            
            // Not really sure at the moment
            'access_params' => 'accessName',

            // None, one or more params that you need for this request
            // They will be passed IN THIS SAME ORDER to the function that you
            // indicate in 'action'
            'params' => array(
            array(

                "key" => "",
                // Default value if not supplied, only applies if not required
                "def" => "",
                // Required? If false and not provided in request I will
                // use the default value, if true and not provided in
                // request I will return error and not proceed with the request
                "req" => true
                )),
            // File where the function to call is located
            // Always put the functions in a different file, at least as much
            // as possible
            'file' => 'file.php'
        )
    );

    return $paths;
}

/**
 * Get the perms for this module.
 * Not fully implemented yet.
 */
function MODULENAME_access()
{
    $perms = array(
        array(
            // A human readable name
            'name'        => 'Do something with this module',
            // Something to remember what it is for
            'description' => 'What can be achieved with this permission',
            // Internal machine name, no spaces, no funny symbols, same rules as a variable
            // Use yourmodule_ prefix
            'code'        => 'mymodule_access_one',
            // Default value in case it is not set
            'def'        => false, //Or true, you decide
        ),
    );
}

/**@}*/
/** @}*/
