<?php

/**
 * Generate and return a token
 *
 * Token format: idDomain_idUser_secret
 *
 * @workingOnIt
 */
function lestatz_tokenGet(
    $idDomain,
    $idUser,
    $token
) {
    grace_debug('Generate token: ' . $idUser . ' / ' . $idDomain);

    include_once('encrypt.php');
    include_once('tools.php');

    $domain = lestatz_domainGet($idDomain);

    if (!$domain) {
        return tools_errSet(
            'Domain does not exist',
            'ERR_ERR'
        );
    }

    if ($domain['secret'] != $token) {
        return tools_errSet(
            'Wrong token: ' . $domain['secret'] . ' != ' . $token,
            'ERR_ERR'
        );
    }

    $key = conf_get('secret', 'lestatz', '');

    $encrypt = new Chirp\Cryptor($key);

    $tokenEnc = $encrypt->encrypt("{$idDomain}_{$idUser}_{$key}");

    if (!$tokenEnc) {
        return tools_errSet(
            'Unable to encrypct the token',
            'ERR_ERR'
        );
    }

    grace_debug('Token: ' . $tokenEnc);

    return $tokenEnc;
}
