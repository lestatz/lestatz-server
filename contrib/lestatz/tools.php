<?php

/**
 *  Get the details about a domain.
 */
function _lestatz_getDomainDets(
    $domain,
    $what = 'code'
) {
    $q = "SELECT *
		FROM `lestatz_domains` d
		WHERE d.`$what` = '$domain'";

    $results = db_querySingle($q);

    if (_db_queryGood($results)) {
        return $results;
    }

    return ['idDomain' => 0];
}

# Create/Store goals
# @todo Do I need idUser in goals?
function _lestatz_storeGoal(
    $goal,
    $idDomain
) {
    grace_debug('Store goals: ' . $goal);

    if ($goal == '') {
        return 0;
    }

    $goalDets = _lestatz_getGoalDets($goal, $idDomain);

    if ($goalDets['idGoal'] > 0) {
        $q = sprintf(
            "UPDATE `lestatz_goals` AS g
			SET g.total = g.total + 1
			WHERE g.idGoal = %s",
            $goalDets['idGoal']
    );

        db_exec($q);

        return $goalDets['idGoal'];
    } else {
        # Insert a new goal
        $q = sprintf(
            "INSERT INTO `lestatz_goals`
			(`idUser`,`goal`,`total`,`idDomain`)
			VALUES ('%s', '%s', '1', '%s');",
            0,
            $goal,
            $idDomain
        );

        db_exec($q);

        #@todo Is this necessary?
        //$goalDets = _lestatz_getGoalDets($goal, $idDomain);

        //return $goalDets['idGoal'];
    }
}

# Get the details about a goal
function _lestatz_getGoalDets(
    $goal,
    $idDomain
) {
    $q = "SELECT *
		FROM `lestatz_goals` AS g
		WHERE g.goal = '$goal'
		AND g.idDomain = '$idDomain'";

    $results = db_querySingle($q);

    if ($results != 'ERROR_DB_NO_RESULTS_FOUND') {
        return $results;
    }

    return array('idGoal' => 0);
}

# Create/Store refs
function _lestatz_storeRef(
    $ref,
    $idDomain
) {
    grace_debug('Check ref: ' . $ref);

    if ($ref == '') {
        return 0;
    }

    $refDets = _lestatz_getRefDets($ref, $idDomain);

    if ($refDets['idRef'] > 0) {
        $q = sprintf(
            "UPDATE `lestatz_refs` AS g
			SET g.total = g.total + 1
			WHERE g.idRef = %s",
            $refDets['idRef']
    );

        db_exec($q);

        return $refDets['idRef'];
    } else {
        # Insert a new ref
        $q = sprintf(
            "INSERT INTO `lestatz_refs`
			(`idUser`, `ref`, `total`, `idDomain`)
			VALUES ('%s', '%s', '1', '%s');",
            0,
            $ref,
            $idDomain
        );

        db_exec($q);

        # @todo Is this necessarry?
        $refDets = _lestatz_getRefDets($ref, $idDomain);

        return $refDets['idRef'];
    }
}

# Get the details about a ref
function _lestatz_getRefDets(
    $ref,
    $idDomain
) {
    $q = sprintf(
        "SELECT r.*
		FROM `lestatz_refs` r
		WHERE r.ref = '%s'
		AND r.idDomain = '%s'",
        $ref,
        $idDomain
    );

    $results = db_querySingle($q);

    if ($results != 'ERROR_DB_NO_RESULTS_FOUND') {
        return $results;
    }

    return array('idRef' => 0);
}

# Anonymise an ipv4
# @todo do this for ipv6
function _lestatz_anonIp($ip)
{
    return substr_replace(
        $ip,
        '.0',
        strrpos($ip, '.'),
        strlen($ip)
    );
}

/**
 * Get location details
 *
 * https://lite.ip2location.com/database-ip-country-region-city-latitude-longitude-zipcode
 */
function loc_get(
    $fake = false
) {
    $ip = $_SERVER['REMOTE_ADDR'];

    # If you are running localhost lets fake an ip
    if ($ip == '127.0.0.1'
         || $ip == '0.0.0.0'
         || $ip == '::1'
         || $fake === true) {
        grace_debug('Faking Ip');
        $ip = long2ip(rand(1780, 4294967295));
    }

    grace_debug('Getting location information for an ip: ' . $ip);

    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
        $dbLitePath = conf_get('loc_db_4', 'lestatz', '');
    } elseif ($ipv6=filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
        $dbLitePath = conf_get('loc_db_6', 'lestatz', '');
    } else {
        $dbLitePath = conf_get('loc_db_4', 'ads', '');
    }

    grace_debug('Using db: ' . $dbLitePath);

    modules_loader('sqlite');

    $dbLite = new MyDBLite($dbLitePath, SQLITE3_OPEN_READONLY);

    if (!$dbLite) {
        grace_err('I could not find or open the sqlite database: ' . $dbLitePath);
        return  _loc_fakeLocation($ip);
    }

    # Ip en número entero
    $ipInt = _loc_ipToInt($ip);

    $q = "SELECT *
		FROM `ip2location`
		where ip_to > '$ipInt'
		LIMIT 1";

    $loc = $dbLite->qSingle($q);

    if (!$loc) {
        grace_error('I could not find information about this IP');
        return  _loc_fakeLocation($ip);
    }
    
    return array(
        'country'   => $loc['country_code'],
        'region'    => $loc['region_name'],
        'city'      => $loc['city_name'],
        'ipAddress' => $ip,
        'longitude' => $loc['longitude'],
        'latitude'  => $loc['latitude'],
        'post'      => $loc['post_code']
    );
}

/**
 * Converts human readable representation to a 128 bit int
 * which can be stored in MySQL using DECIMAL(39,0).
 *
 * Requires PHP to be compiled with IPv6 support.
 * This could be made to work without IPv6 support but
 * I don't think there would be much use for it if PHP
 * doesn't support IPv6.
 *
 * @param string $ip IPv4 or IPv6 address to convert
 * @return string 128 bit string that can be used with DECIMNAL(39,0) or false
 */
function _loc_ipToInt($ip)
{
    // make sure it is an ip
    if (filter_var($ip, FILTER_VALIDATE_IP) === false) {
        return false;
    }

    $parts = unpack('N*', inet_pton($ip));

    // fix IPv4
    if (strpos($ip, '.') !== false) {
        $parts = array(1=>0, 2=>0, 3=>0, 4=>$parts[1]);
    }

    foreach ($parts as &$part) {
        // convert any unsigned ints to signed from unpack.
        // this should be OK as it will be a PHP float not an int
        if ($part < 0) {
            $part += 4294967296;
        }
    }

    // Use BCMath if available
    if (function_exists('bcadd')) {
        $decimal = $parts[4];
        $decimal = bcadd($decimal, bcmul($parts[3], '4294967296'));
        $decimal = bcadd($decimal, bcmul($parts[2], '18446744073709551616'));
        $decimal = bcadd($decimal, bcmul($parts[1], '79228162514264337593543950336'));
    }
    // Otherwise use the pure PHP BigInteger class
    else {
        $decimal = new Math_BigInteger($parts[4]);
        $part3   = new Math_BigInteger($parts[3]);
        $part2   = new Math_BigInteger($parts[2]);
        $part1   = new Math_BigInteger($parts[1]);

        $decimal = $decimal->add($part3->multiply(new Math_BigInteger('4294967296')));
        $decimal = $decimal->add($part2->multiply(new Math_BigInteger('18446744073709551616')));
        $decimal = $decimal->add($part1->multiply(new Math_BigInteger('79228162514264337593543950336')));

        $decimal = $decimal->toString();
    }

    return $decimal;
}
/**
 * Fake a location
 */
function _loc_fakeLocation($ip)
{
    return array(
        'country' => 'crc',
        'region' => 'crc',
        'city' => 'crc',
        'ipAddress' => $ip,
        'longitude' => '20',
        'latitude' => '12',
        'post' => '11111'
    );
}
