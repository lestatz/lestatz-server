# About
LeStatz module, register statistics about your website in real time and in a versatile way, this means that you can have stats for each of your users and 
for a global account, aka the entire website.
It respects the 'do not track' config from the browser and has a very low footprint.

# Features
* Multiuser: You may have several users or just track for several users, then, from your CMS give the option to view individual statz.
* Automatic creation of goals: Just add ?goal=newGoal and I will track it for you

# How to register stats in your pages
In the server:
Move publicLeStatz.min.js to a public location like http://lestatz.example.com/publicLeStatz.js or http://example.com/lestatz/publicLeStatz.js

In your pages:
Just include this in your pages and set the correct values for the indicated variables.
You can create several divs and call LeStatz several times,

<!-- Lestatz Web Analytics -->
<div id="LeStatz"></div>
<script type="text/javascript" src="http://example.com/publicLeStatz.js"></script>
<script type="text/javascript">
    LeStatz.boot(1, 'LeStatz', 'http://example.com/lestatz/');
</script>
<!-- //Lestatz -->

# Drupal example
In this case you will call LeStatz with the id of the owner of the node, this is usefull if you want to
provide all of your users with their own statz.
Following this example you can create a very powerfull statz system for your website.
I might create a module sometime or just improve this example.

[code]
<?php
# Confirm that it is a node
if(is_numeric(arg(1))){
  $node = node_load(arg(1));
  $uid = $node->uid;
}else{
  # Use the admin id by default for all other pages
  $uid = 1;
}
?>
[/code]

<!-- LeStatz Web Analytics -->
<div id="LeStatz"></div>
<script type="text/javascript" src="http://<?php print $_SERVER['HTTP_HOST']; ?>/path/to/lestatz/js/publicLeStatz.js"></script>
<script type="text/javascript">
    LeStatz.boot(<?php print $uid; ?>, 'LeStatz', 'http://<?php print $_SERVER[\'HTTP_HOST\']; ?>');
</script>
<!-- //LeStatz -->
 // end of Drupal example



