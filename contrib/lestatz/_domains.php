<?php

/**
 * Get websites for a user.
 *
 * External call.
 */
function lestatz_sitesGet()
{
    global $user;

    grace_debug("Getting a list of sites for a user");

    # Unique visitors
    $q = sprintf(
        'SELECT *
		FROM `lestatz_domains`
		WHERE idUser = %s
		ORDER BY idDomain DESC',
        $user['idUser']
    );

    $sites = db_q($q);

    if ($sites == false || $sites == 'ERROR_DB_ERROR') {
        return false;
    }

    return $sites;
}

/**
 * Get domain information, this is only for MY domains.
 *
 * External call.
 */
function lestatz_domainGetDetails($idDomain)
{
    global $user;

    grace_debug('Getting domain informationa');

    $domain = lestatz_domainLoad($idDomain, $user);

    if ($domain['idUser'] != $user['idUser']) {
        return tools_errSet(
            'The domain does not belong to this user',
            'ERR_ERR'
         );
    }

    return $domain;
}

/**
 * Load a domain
 */
function lestatz_domainLoad($idDomain, $user)
{
    $q = "SELECT *
		FROM `lestatz_domains`
		WHERE idDomain = $idDomain";

    $domain = db_querySingle($q);

    if ($domain == false || $domain == 'ERROR_DB_ERROR') {
        return false;
    }

    $domain['token'] = lestatz_tokenDomainGen(
        $user,
        $domain['domain']
    );

    return $domain;
}

/**
 * Create a new domain
 */
function lestatz_domainNew(
    $idDomain,
    $domain,
    $name,
    $secret,
    $timezone
) {
    global $user;

    grace_debug('Register or edit a domain');
     
    if (!filter_var($domain, FILTER_VALIDATE_DOMAIN)) {
        return tools_errSet(
            'Wrong domain',
            'ERR_ERR'
        );
    }
     
    if (!is_numeric($idDomain)) {
        return tools_errSet(
            'Wrong idDomain',
            'ERR_ERR'
        );
    }
    if (!is_numeric($timezone)) {
        return tools_errSet(
            'Wrong timezone',
            'ERR_ERR'
        );
    }

    if ($idDomain > 0) {
        grace_info('It looks like you would like to edit');

        $c = lestatz_domainLoad($idDomain, $user);

        if (!$c) {
            return tools_errSet('Comp not found', 'ERR_IRL_COMP_NOT_FOUND');
        }

        if ($c['idUser'] != $user['idUser']) {
            return tools_errSet('User is not allowed to edit this company', 'ERR_IRL_NO_ACCESS');
        }

        return _lestatz_domainUpdate(
            array(
                'idUser' => $user['idUser'],
                'domain' => $domain,
                'name' => $name,
                'secret' => $secret,
                'timezone' => $timezone,
                'idDomain' => $idDomain
            )
        );
    }

    #@todo Check that the url does not exist, it is a unique key in the
    # database so it will give error if you try to add it anyway, but it
    # would be nice to know about it specifically.

    $d = _lestatz_domainNew(
        array(
            'idUser' => $user['idUser'],
            'domain' => $domain,
            'name' => $name,
            'secret' => $secret,
            'timezone' => $timezone
        )
      );

    if (!$d) {
        return 'ERR_ERR';
    }
    return 'ALL_GOOD';
}


/**
 * Actually create a new domain
 */
function _lestatz_domainNew($domain)
{
    $q = sprintf(
        "INSERT INTO `lestatz_domains` (`idUser`,`domain`,`name`,`secret`,`timezone`,`created`)
		VALUES('%s','%s','%s','%s','%s','%s')",
        $domain['idUser'],
        db_escape($domain['domain']),
        db_escape($domain['name']),
        db_escape($domain['secret']),
        $domain['timezone'],
        time()
      );

    $r = db_exec($q);

    if ($r == false || $r == 'DB_ERROR') {
        return false;
    }

    return true;
}

/**
 * Actually update a domain
 */
function _lestatz_domainUpdate(
    $domain
) {
    grace_info('Actually updating the domain');

    $q = sprintf(
        "UPDATE `lestatz_domains`
		set idUser='%s',
		domain='%s',
		name='%s',
		secret='%s',
		timezone='%s'
		WHERE idDomain = '%s'",
        $domain['idUser'],
        db_escape($domain['domain']),
        db_escape($domain['name']),
        db_escape($domain['secret']),
        db_escape($domain['timezone']),
        $domain['idDomain']
    );

    $r = db_exec($q);

    if ($r < 0) {
        return tools_errSet(
            'There was an error trying to update the domain',
            'ERR_ERR'
         );
    }

    return 'ALL_GOOD';
}

/**
 * Edit a domain
 */
