<?php

/**
 * Get current live visitors.
 *
 * External call.
 */
function lestatz_liveVisitors(
    $idSite
) {
    global $user, $domain;

    grace_debug("Getting live visitors for $idSite");

    include_once('tools.php');

    $visitors = array(
        'unique' => 0,
        'total' => 0
     );

    $time = time() - 60;

    # Do I own the site? or am I just a passer by
    $w = '';
    if ($domain['owner'] == 'n') {
        grace_debug('I am not the owner, I will only get MY stats');
        $w = " AND `idOwner` = '" . $user['lestatzUserId'] . "'";
    }

    # Unique visitors
    $q = sprintf(
        'SELECT COUNT(DISTINCT(visitorCode)) AS uniqueV
			FROM `lestatz_visit_log`
			WHERE timestamp > %s
			AND domainId = %s %s
			ORDER BY idVisit DESC',
        $time,
        $domain['idDomain'],
        $w
    );

    $liveUnique = db_querySingle($q);

    if ($liveUnique != false && $liveUnique != 'ERROR_DB_ERROR') {
        $visitors['unique'] = $liveUnique['uniqueV'];
    }

    # Unique visitors
    $q = sprintf(
        'SELECT COUNT(visitorCode) AS visitors
			FROM `lestatz_visit_log`
			WHERE timestamp > %s
			AND domainId = %s %s
			ORDER BY idVisit DESC',
        $time,
        $domain['idDomain'],
        $w
    );

    $live = db_querySingle($q);
    grace_debug('-->' . JSON_encode($live));
    if ($live != false && $live != 'ERROR_DB_ERROR') {
        $visitors['total'] = $live['visitors'];
    }

    return $visitors;
}
