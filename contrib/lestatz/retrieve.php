<?php

/**
 * Retrieve statz for a site and for a user.
 *
 * External call.
 */
function lestatz_retrieve(
    $last,
    $idDomain,
    $type,
    $ini,
    $end
) {
    global $user, $domain;

    grace_debug("Getting your statz => last: $last | domain: $idDomain");

    include_once('tools.php');

    # Do I own the site? or am I just a passer by
    $w = [];
    if ($domain['owner'] == 'n') {
        grace_debug('I am not the owner, I will only get MY stats');
        $w[] = "`idOwner` = '{$user['lestatzUserId']}'";
    }

    ##
    # Presets
    ##

    if ($type == 'preset' || $type == 'file') {
        grace_debug('Preset based request: ' . $ini);

        $presets = [
            '1H' => '1 hour ago',
            '6H' => '6 hours ago',
            '12H' => '6 hours ago',
            'today' => 'yesterday midnight',
            '1w' => '1 week ago',
            '2w' => '2 weeks ago',
            '1m' => '1 month ago',
        ];

        if (!array_key_exists($ini, $presets)) {
            return tools_errSet(
                'The preset does not exist',
                'ERR_ERR'
            );
        }

        $w = [
            'timestamp > ' . strtotime($presets[$ini])
        ];

        $log = _lestatz_logLoad(
            $domain['idDomain'],
            $w
          );

        # Download?
        if ($type == 'file') {
            params_set('replyType', 'attach');
        }
    } else {

         # Last visit?
        $limit = '';
        if ($last != 0) {
            $w[] =  "idVisit > $last";
        } else {
            $limit = 'LIMIT 10';
        }

        $log = _lestatz_logLoad(
            $domain['idDomain'],
            $w,
            $limit
         );
    }

    //_lestatz_printResp($log);

    if (!$log) {
        grace_debug('No logs found');
        $log = [
             'time' => time(),
             'log' => false
         ];
    }

    $log = [
         'time' => time(),
         'log' => $log
     ];

    /*
         file_put_contents('/home/x/devNew/lestatz/dev/server/logs/logS.log', JSON_encode($log));

         return [
                     'time' => time(),
                     'log' => false
               ];

     */
    if ($type == 'file') {
        grace_debug('Download log');
        return [
               'fileName' => time() . '.csv',
               'text' => JSON_encode($log) //_lestatz_logToCSV($log)
         ];
    }

    return $log;
}

/**
 * Retrieve statz for a site and for a user.
 *
 * External call.
 * @deprecated?
 */
function _lestatz_retrieve(
    $last,
    $idDomain,
    $type
) {
    global $user, $domain;

    grace_debug("Getting your statz => last: $last | domain: $idDomain");

    include_once('tools.php');

    # Do I own the site? or am I just a passer by
    $w = '';
    if ($domain['owner'] == 'n') {
        grace_debug('I am not the owner, I will only get MY stats');
        $w = " AND `idOwner` = '" . $user['lestatzUserId'] . "'";
    }

    # Last visit?
    $limit = '';
    if ($last != 0) {
        $w .=  " AND idVisit > $last";
    } else {
        $limit = 'LIMIT 10';
    }

    $q = sprintf(
        'SELECT * 
		FROM `lestatz_visit_log`  
		WHERE domainId = %s 
		%s 
		ORDER BY idVisit DESC
		%s',
        $domain['idDomain'],
        $w,
        $limit
        );

    $log = db_q($q);

    if (!$log || $log == 'ERROR_DB_ERROR') {
        return [
                'time' => time(),
                'log' => false
            ];
    }

    return [
            'time' => time(),
            'log' => $log
        ];
}

/**
 * Retrieve log for a site. And a user?
 *
 * External call.
 * @deprecated?
 */
function lestatz_logLoad(
    $idDomain
) {
    global $user, $domain;

    grace_debug("Loading a log in domain: $idDomain");

    include_once('tools.php');

    # Do I own the site? or am I just a passer by
    $w = [];
    if ($domain['owner'] == 'n') {
        grace_debug('I am not the owner, I will only get MY stats');
        $w['owner'] = "`idOwner` = '" . $user['lestatzUserId'] . "'";
    }

    $dates = lestatz_datesSet();

    $w['ini'] = 'timestamp > ' . $dates['ini'];

    if ($dates['end'] != 0) {
        $w['end'] = 'timestamp < ' . $dates['end'];
    }

    $logs = _lestatz_logLoad($idDomain, $w);

    return $logs;
}

/**
 * Helper function to load logs.
 * \moved
 */
function _lestatz_logLoad(
    $idDomain,
    $w,
    $limit = ''
) {
    if (count($w) > 0) {
        $w = 'AND ' . implode(' AND ', $w);
    } else {
        $w = '';
    }

    $q = sprintf(
        'SELECT * 
		FROM `lestatz_visit_log`  
		WHERE domainId = %s 
		%s 
		ORDER BY idVisit DESC
		%s',
        $idDomain,
        $w,
        $limit
        );

    $log = db_q($q);

    if (!$log || $log == 'ERROR_DB_ERROR') {
        return false;
    }

    return $log;
}

/**
 * Convert a log into a csv
 */
function _lestatz_logToCSV(&$log)
{
    grace_debug('Convert stats log to csv');

    // Lenght/size of the report
    $l = count($log);

    grace_debug("A total of $l stats where recieved");

    $logCsv = '';

    // Headers
    $headers = array_keys($log[0]);

    $logCsv = '"' . implode('","', $headers) . '"' . "\n";

    $values = [];

    for ($i = 0; $i < $l; $i++) {
        $values = $log[$i];

        $logCsv .= '"' . implode('","', $values) . '"' . "\n";
    }

    /*

             // Visitors
             $v = [];

             // Refs
             $r = [];

             // Goals
             $g = [];

             // Pages
             $p = [];

             // Browsers
             $b = [];

             // Countries
             $c = [];

             // OS
             $o = [];

             for ($i = 0; $i < $l; $i++) {

                     // Visitors
                     $v[$i] = $log[$i]['visitorCode'];

                     // Refs
                     if ($log[$i]['ref'] != '') {
                                $r[$i] = $log[$i]['ref'];
                     }

                     // Goals
                     if ($log[$i]['goal'] != '') {
                                $g[$i] = $log[$i]['goal'];
                     }

                     // Page Names
                     $p[$i] = $log[$i]['pageName'];

                     // Browsers
                     $b[$log[$i]['browser']] = !isset($b[$log[$i]['browser']])
                                ? 1
                                : $b[$log[$i]['browser']] + 1;

                     // Countries
                     $c[$log[$i]['country']] = !isset($c[$log[$i]['country']])
                                ? 1
                                : $c[$log[$i]['country']] + 1;

                     // OS
                     $o[$log[$i]['os']] = !isset($o[$log[$i]['os']])
                                ? 1
                                : $o[$log[$i]['os']] + 1;
             }

             // Unique visitors
             $uniqueV = array_unique($v);

             // Unique refs
             $uniqueR = array_unique($r);

             // Unique goals
             $uniqueG = array_unique($g);

             // Last visit
             //$lastTime = Cala.dateParse($log[l - 1].timestamp, 'full');
             $lastTime = $log[$l - 1]['timestamp'];

             // Unique pages
             $uniqueP = array_unique($p);

             // Unique browsers
             $browserList = array_keys($b);

             // Unique countries
             $countriesList = array_keys($c);

             // Unique OSs
             $osList = array_keys($o);

             $anal = [

                     // Last visit time
                     'lastTime' => $lastTime,

                     // Total unique visitors
                     'visitorsUniqueTotal' => count($uniqueV),
                     'visitorsUniqueTotalP' => (count($uniqueV)/$l)*100,

                     // Total visitors/visits
                     'visitorsTotal' => $l,

                     // List of unique visitors
                     'visitorsUnique' => $uniqueV,

                     //
                     // Refs
                     //
                     // Total visits that have a ref
                     'refsTotal' => count($r),
                     'refsTotalP' => (count($r)/$l)*100,
                     // Total unique visits that have a ref
                     'refsUniqueTotal' => count($uniqueR),
                     'refsUniqueTotalP' => (count($uniqueR)/count($r)*100),
                     // List of unique refs
                     'refsUnique' => $uniqueR,

                     //
                     // Goals
                     //
                     // Total visits that have a ref
                     'goalsTotal' => count($g),
                     'goalsTotalP' => (count($g)/$l)*100,
                     // Total unique visits that have a ref
                     'goalsUniqueTotal' => count($uniqueG),
                     'goalsUniqueTotalP' => (count($uniqueG)/count($g))*100,
                     // List of unique refs
                     'goalsUnique' => $uniqueG,

                     // Pages
                     'pagesUnique' => count($uniqueP),

                     // Browsers
                     'browsers' => $b,
                     'browsersList' => $browserList,
                     'browsersUniqueTotal' => count($browserList),

                     // Countries
                     'countries' => $c,
                     'countriesList' => $countriesList,
                     'countriesUniqueTotal' => count($countriesList),

                     // OS
                     'os' => $o,
                     'osList' => $osList,
                     'osUniqueTotal' => count($osList)

             ];
     */
    return $logCsv;
}

/**
 * Print the results in json format.
 */
function _lestatz_printResp($log)
{
    tools_addHeaders(true);

    print '{"resp":{"time":'.time().',"log":[';
    $first = true;
    // --
    while ($row = $log->fetch_assoc()) {
        if (!$first) {
            print ',';
        }
        $first = false;
        print JSON_encode($row);
        //$j = JSON_encode($row);
        //echo substr($j, 1, strlen($j) -1);
    }
    print ']}}';

    //echo(memory_get_peak_usage() / 1000000);
    // --
    exit;
}
