/*!
 * Public stuff for LeStatz
 * v 0.3.0-stable
 * (c) 2019~ Twisted Head
 * Licence: Do no harm. You may use this software for free as long as it is
 * NOT to harm ANY living creature.
 */

var LeStatz = LeStatz || {};

var LeStatz = {

	url: "https://lestatz.e2go.org",

	// Browser info
	browserInfo: '',

	session: '',

	dets: {},

	// Generate a new session if necessary
	sessionGen: function(){

		if(LeStatz.session == ''){
			// I need a new session
			LeStatz.session = Math.round(Math.random() * Math.floor(1464578465213521));
		}

		return LeStatz.session;

	},

	varGet: function(v, d){
		if(v !== undefined && v != false){
			return v;
		}
		return d;
	},

	visitorGet: function(force = false){

		if(force === true){
			id = null;
		}else{
			var id = window.localStorage.getItem('LeStatz_id');
		}

		if(id === null
			|| id === undefined
			|| id === 'undefined'
			|| id === 'null'
			){
			// Generate id
			id = Math.round(Date.now() * (Math.random() * 1));
			window.localStorage.setItem('LeStatz_id', id);
		}
		return id;
	},

	//http://www.netlobo.com/url_query_string_javascript.html
	getParam: function(name){
		name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
		var regexS = "[\\?&]"+name+"=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);
		if(results === null)
			return "";
		else
			return results[1];
	},
	boot: function(p){
		if(window.navigator.doNotTrack == 'yes'){
			//I will do nothing
			console.log("Respecting your privacy settings, I will not track you. ~LeStatz");
		}
		else{

			// Browser info
			//https://github.com/faisalman/ua-parser-js
			var LeStatz_parser = new UAParser();
			LeStatz.browserInfo = LeStatz_parser.getResult();

			//Structured like this just to make it more readable
			LeStatz.dets = {
				w: "lestatz_store_statz",

				domainToken: p.domainToken,

				url: document.URL,

				idOwner: LeStatz.varGet(p.idOwner, 0),

				title: LeStatz.varGet(p.title, document.title),

				visitorCode: LeStatz.varGet(p.visitorCode, LeStatz.visitorGet(true)),

				session: LeStatz.sessionGen(),

				// Browser lang
				lang: navigator.language,

				referer: document.referrer,

				browserInfo: LeStatz.browserInfo,
				browserLang: navigator.language,
				agent: navigator.userAgent,
				browserV: navigator.appVersion,
				os: navigator.platform,

				goal: LeStatz.varGet(p.goal, this.getParam('goal')),

				ref: LeStatz.varGet(p.ref, this.getParam('ref')),

				domain: window.location.host,

				extras: LeStatz.varGet(p.extras, JSON.stringify({})),

				fakeIp: LeStatz.varGet(p.fake, false),
			};

			// User has a custom path?
			this.url = LeStatz.varGet(p.server, this.url);

			data = "w=lestatz_store_statz&x="
				+ btoa(JSON.stringify(LeStatz.dets));

			var msgs = "Loading LeStatz, we are tracking this information: >> " + JSON.stringify(LeStatz.dets);

			msgs = msgs + "We will also track your IP and it will be anonymized to comply with GDPR. \n If you are not comfortable with this, you may set your 'Do not track' option to 'on'. \n Maybe Safari will not comply with this.";

			msgs += "Register stats at: " + this.url;

			console.log(msgs);

			var xhttp = new XMLHttpRequest();

			xhttp.open("POST", this.url);
			xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			xhttp.send(data);

			return msgs;
		}
	}
};


