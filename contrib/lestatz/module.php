<?php
/** @file lestatz/module.php
 * LeStatz Web Analitics
 * Analize your web on your own terms.
 */

/**
 * \defgroup LeStatz
 * @{
 */

# The domain in question
global $domain;

/**
 * Boot up procedure
 */
function lestatz_bootMeUp()
{
    // Just booting up
    grace_debug("LeStatz is in the house!");
}

/**
 * Init function
 */
function lestatz_init()
{
    $paths = array(
        array(
            'r' => 'lestatz_store_statz',
            'action' => 'lestatz_storeStatz',
            'access' => 'users_openAccess',
            'params' => array(
                //array("key" => "x", "def" => "", "req" => true)
            )),

        // Get domain information
        array(
            'r' => '__lestatz_domain_get_details',
            'action' => 'lestatz_domainGetDetails',
            'access' => 'users_loggedIn',
            'params' => array(
                array('key' => 'idDomain', 'req' => true)
            ),
            'file' => 'domains.php'
        ),

        // Retrieve statz for a site and for a user
        array(
            'r' => '___lestatz_retrieve',
            'action' => 'lestatz_retrieve',
            'access' => 'lestatz_userAccess',
            'access_params' => array(
                array('key' => 'userToken', 'def' => '', 'req' => false),
                array('key' => 'idDomain', 'req' => true)

            ),
            'params' => array(
                array('key' => 'last', 'req' => true),
                array('key' => 'idDomain', 'req' => true),
                array('key' => 'type', 'def' => 'live', 'req' => false),
                array('key' => 'ini', 'def' => '', 'req' => false),
                array('key' => 'end', 'def' => '', 'req' => false)
            ),
            'file' => 'retrieve.php'
        ),

        // Load logs
        // @deprecated?
        array(
            'r' => 'lestatz_log_load',
            'action' => 'lestatz_logLoad',
            'access' => 'lestatz_userAccess',
            'access_params' => array(
                array('key' => 'userToken', 'def' => '', 'req' => false),
                array('key' => 'idDomain', 'req' => true)
            ),
            'params' => array(
                array("key" => 'idDomain', 'def' => 0, 'req' => false)
            ),
            'file' => 'retrieve.php'
        ),

        // Get a user token
        array(
            'r' => 'lestatz_token_get',
            'action' => 'lestatz_tokenGet',
            'access' => 'lestatz_tokenAccess',
            'access_params' => array(
                array('key' => 'idDomain', 'req' => true),
                array('key' => 'secret', 'req' => true)
            ),
            'params' => array(
                array('key' => 'idDomain', 'req' => true),
                array('key' => 'idOwner', 'req' => true),
                array('key' => 'secret', 'req' => true)
            ),
            'file' => 'tokenGet.php'),

        // Add a new domain
        array(
            'r' => 'lestatz_domain_new',
            'action' => 'lestatz_domainNew',
            'access' => 'users_loggedIn',
            'params' => array(
                array("key" => 'idDomain', 'req' => true),
                array("key" => 'domain', 'req' => true),
                array("key" => 'name', 'req' => true),
                array("key" => 'secret', 'req' => true),
                array("key" => 'timezone', 'req' => true)
            ),
            'file' => 'domains.php'));

    return $paths;
}

/**
 * Get the perms for this module
 */
function lestatz_access()
{
    $perms = array(
        array(
            # A human readable name
            'name' => 'Register LeStatz',
            # Something to remember what it is for
            'description' => '',
            # Internal machine name, no spaces, no funny symbols, same rules as a variable
            # Use yourmodule_ prefix
            'code' => 'mymodule_access_one',
            # Default value in case it is not set
            'def' => false, //Or true, you decide
        ),
    );
}

/**
 * Set ini and end date/times
 */
function lestatz_datesSet()
{
    $dates = [];

    $dates['ini'] = lestatz_datesGetIni();
    $dates['end'] = lestatz_datesGetEnd();

    return $dates;
}

/**
 * Gets/Generates the start date
 */
//@deprecated
function lestatz_datesGetIni()
{

    # If none is set I will start today at midnight
    $t = strtotime("yesterday midnight");
    return params_get('ini', $t);
}

/**
 * Gets/Generates the end date
 */
function lestatz_datesGetEnd()
{

    # If no date is set, I will go for today at midnight
    return params_get('end', 0); //mktime(0, 0, 0));
}

/**
 * Load statistics and return the values.
 * @todo report by site, goal, ref and combinations
 */
function lestatz_reportsLoad()
{
    global $user;

    grace_debug("Loading reports");

    # @todo Add other admins who can see all statz.
    # Only admins can change the requested user.
    if ($user->idUser == 1) {
        # Use 0 if you want to see all users
        $byUser = (params_get('idUser', 0) > 0) ? ' AND user_id = ' . params_get('idUser', '0') : '';
    } else {
        $byUser = " AND user_id = " . $user->idUser;
    }

    $type = params_get('type', 'latest');

    # I should change this to nothing requested
    $results = ERROR_DB_NO_RESULTS_FOUND;

    if ($type == 'latest') {
        $results = db_query(sprintf(
            "SELECT vl.*, g.goal, r.ref
			FROM lestatz_visit_log vl
			LEFT JOIN lestatz_goals g on g.idGoal = vl.idGoal
			LEFT JOIN lestatz_refs r on r.idRef = vl.idRef
			WHERE timestamp >= %s
			AND timestamp <= %s
			%s
			ORDER BY timestamp DESC",
            lestatz_datesGetIni(),
            lestatz_datesGetEnd(),
            $byUser
        ), 2);

        # Parse dates
        if ($results != ERROR_DB_NO_RESULTS_FOUND) {
            grace_debug("Formating dates");

            foreach ($results as $row) {
                $row->dateFormated = date_parseDate($row->timestamp);
            }
        }
    } elseif ($type == 'unique') {
        $results = db_query(sprintf(
            "SELECT COUNT(DISTINCT(ip)) as visits, FROM_UNIXTIME(timestamp, '%%Y-%%c-%%d') as the_day
			FROM lestatz_visit_log
			WHERE timestamp >= %s
			AND timestamp <= %s
			%s
			GROUP BY the_day",
            lestatz_datesGetIni(),
            lestatz_datesGetEnd(),
            $byUser
            ), 2);

        $visits = array();

        $lines  = array();
        $labels = array();

        if ($results != ERROR_DB_NO_RESULTS_FOUND) {
            grace_debug("Found some visitors statz, unique");

            foreach ($results as $row) {
                $labels[] = $row->the_day;
                $data[]   = $row->visits;
            }

            $results = array("labels" => $labels, "data" => $data);
        }
    } elseif ($type == 'total') {
        $results = db_query(sprintf(
            "SELECT count(timestamp) as visits, FROM_UNIXTIME(timestamp, '%%Y-%%d-%%c') as the_day
			FROM lestatz_visit_log
			WHERE timestamp >= %s
			AND timestamp <= %s
			%s
			GROUP BY the_day",
            lestatz_datesGetIni(),
            lestatz_datesGetEnd(),
            $byUser
                ), 2);


        # Declare empty to avoid problems
        $data  = array();
        $labels = array();

        if ($results != ERROR_DB_NO_RESULTS_FOUND) {
            grace_debug("Found some visitors statz, total");

            foreach ($results as $row) {
                $labels[] = $row->the_day;
                $data[]   = $row->visits;
            }

            $results = array("labels" => $labels, "data" => $data);
        }
    } elseif ($type == 'browser') {
        $results = db_query(sprintf(
            "SELECT COUNT(timestamp) as visits, browser
			FROM lestatz_visit_log
			WHERE timestamp >= %s
			AND timestamp <= %s
			%s
			GROUP BY browser
			ORDER BY visits DESC",
            lestatz_datesGetIni(),
            lestatz_datesGetEnd(),
            $byUser
                    ), 2);

        if ($results != ERROR_DB_NO_RESULTS_FOUND) {
            grace_debug("Found some visitors statz, browser");

            # Assign a random colour per browser, it would be nice to have specific custom colours
            foreach ($results as $row) {
                //$data[] = sprintf('{value: %s, label: "%s", color: "#%s"}', $row->visits, $row->browser, strtoupper(dechex(rand(0,10000000))));
                $data[] = array('value' =>  $row->visits,
                                'label' => $row->browser,
                                'color' => '#' . strtoupper(dechex(rand(0, 10000000)))
                            );
            }

            $results = $data;
        }
    } elseif ($type == 'country') {
        $results = db_query(sprintf(
            "SELECT COUNT(timestamp) as visits, country
			FROM lestatz_visit_log
			WHERE timestamp >= %s
			AND timestamp <= %s
			%s
			GROUP BY country
			ORDER BY visits DESC",
            lestatz_datesGetIni(),
            lestatz_datesGetEnd(),
            $byUser
                        ), 2);

        if ($results != ERROR_DB_NO_RESULTS_FOUND) {
            grace_debug("Found some visitors statz, country");

            # Assign a random colour per browser, it would be nice to have specific custom colours
            foreach ($results as $row) {
                $data[] = array('value' =>  $row->visits,
                                    'label' => $row->country,
                                    'color' => '#' . strtoupper(dechex(rand(0, 10000000)))
                                );
            }

            $results = $data;
        }
    } elseif ($type == 'map_latest') {
        $results = db_query(sprintf(
            "SELECT vl.coordinates, vl.timestamp
			FROM lestatz_visit_log vl
			WHERE timestamp >= %s
			AND timestamp <= %s
			AND (coordinates != ','
			AND coordinates != '')
			%s
			GROUP BY coordinates
			ORDER BY timestamp DESC",
            lestatz_datesGetIni(),
            lestatz_datesGetEnd(),
            $byUser
                            ), 2);

        if ($results != ERROR_DB_NO_RESULTS_FOUND) {
            grace_debug("Found some visitors statz, map");

            # Assign a random colour per browser, it would be nice to have specific custom colours
            foreach ($results as $record) {
                $dats[] = sprintf(
                    '{"type":"Feature","id":"%s","properties":{"name":"90"},"geometry":{"type":"Point","coordinates":[%s]}}',
                    $record->timestamp,
                    $record->coordinates
                                    );
            }

            $dats = implode(",", $dats);

            return '{"type":"FeatureCollection","features":['.$dats.']}';
        }
    }
    return $results;
}

/**
 * Check user login token
 *
 * Token format: idUser_random
 *
 * @workingOnIt
 */
function lestatz_userLoginTokenValidate($userToken)
{
    grace_debug('Confirm token: ' . $userToken);

    include_once('encrypt.php');

    $key = conf_get('secret', 'lestatz', '');

    $encrypt = new Chirp\Cryptor($key);

    $tokenDec = $encrypt->decrypt($userToken);

    if (!$tokenDec) {
        grace_error('Unable to decrypt the token');
        return false;
    }

    $dets = explode('_', $tokenDec);

    if (count($dets) != 3) {
        grace_error('Wrong token');
        return false;
    }

    grace_debug('Token: ' . $tokenDec);

    return $dets;
}

/**
 * Check login for the user.
 *
 * @workingOnIt
 */
function lestatz_userLoginAccess(
    $userToken
) {
    global $user;

    include_once('tools.php');

    grace_debug('Check user`s access for LeStatz');

    if ($user['idUser'] == 0) {
        grace_debug('Not logged in, use a token.');

        # Is the token valid?
        $tokenDets = lestatz_userLoginTokenValidate($userToken);

        if (!$tokenDets) {
            tools_errSet(
                'The token is wrong',
                'ERR_ERR'
            );
            return false;
        }
        $user['lestatzUserId'] = $tokenDets[1];
    } else {
        grace_debug('User is logged in');

        $user['lestatzUserId'] = $user['idUser'];
    }

    return true;
}


/**
 * Check user token
 *
 * Token format: idSite_idUser_secret
 */
function lestatz_userTokenValidate($userToken)
{
    grace_debug('Confirm token: ' . $userToken);

    include_once('encrypt.php');

    $key = conf_get('secret', 'lestatz', '');

    $encrypt = new Chirp\Cryptor($key);

    $tokenDec = $encrypt->decrypt($userToken);

    if (!$tokenDec) {
        grace_error('Unable to decrypt the token');
        return false;
    }

    $dets = explode('_', $tokenDec);

    if (count($dets) != 3) {
        grace_error('Wrong token');
        return false;
    }

    grace_debug('Token: ' . $tokenDec);

    return $dets;
}

/**
 * Check perms for the user.
 */
function lestatz_userAccess(
    $userToken,
    $idDomain
) {
    global $user, $domain;

    include_once('tools.php');

    grace_debug('Check user`s access for LeStatz');

    # Mark domain as not mine by default
    $owner = 'n';

    if ($user['idUser'] == 0) {
        grace_debug('Requesting information for a site I do not own');

        if ($userToken == '') {
            tools_errSet(
                'No token provided',
                'ERR_ERR'
            );
            return false;
        }

        # Is the token valid?
        $tokenDets = lestatz_userTokenValidate($userToken);

        if (!$tokenDets) {
            tools_errSet(
                'The token is wrong',
                'ERR_ERR'
            );
            return false;
        }

        $user['lestatzUserId'] = $tokenDets[1];

        $domain = lestatz_domainGet($tokenDets[0]);

        if (!$domain) {
            tools_errSet(
                'Domain does not exist',
                'ERR_ERR'
            );

            return false;
        }
    } else {
        grace_debug('Request information for a site I own');

        $user['lestatzUserId'] = $user['idUser'];

        $domain = lestatz_domainGet($idDomain);

        if (!$domain) {
            tools_errSet(
                'Domain does not exist',
                'ERR_ERR'
            );

            return false;
        }

        if ($domain['idUser'] != $user['idUser']) {
            tools_errSet(
                'User does not own this site',
                'ERR_ERR'
            );
            return false;
        }

        $owner = 'y';
    }

    $domain['owner'] = $owner;

    return true;
}

/**
 * Check permissions to get a token.
 * @todo!
 */
function lestatz_tokenAccess(
    $idDomain,
    $token
) {
    return true;
}

/**
 * Generate a domain token
 */
function _lestatz_tokenDomainGen(
    $user,
    $domain
) {
    return base64_encode($user['userName'] . '_' . $domain);
}

/**
 * Main function to store statz.
 *
 * I will try to speed up things in here, this must be a VERY fast interaction.
 */
function lestatz_storeStatz()
{

    # Load my tools
    include_once('tools.php');

    # Just in case
    date_default_timezone_set("UTC");

    $dets = [];
    foreach ($_POST as $d => $data) {
        if ($d == 'browserInfo') {
            $dets[$d] = JSON_decode($data);
        } else {
            $dets[$d] = $data;
        }
    }
     
    grace_debug('Decoded dets: ' . JSON_encode($dets));

    # Visitor code
    if (!isset($dets['visitorCode'])) {
        $dets['visitorCode'] = 0;
    }

    if (!is_numeric($dets['visitorCode'])) {
        grace_error('Visitor code is not numeric. Possible attack? ' . $dets['visitorCode']);
        $dets['visitorCode'] = 0;
    }

    # User / Owner of the page
    if (!isset($dets['idOwner'])) {
        $dets['idOwner'] = 0;
    }

    if (!is_numeric($dets['idOwner'])) {
        grace_error('This user does not exist, I will set the page owner to none. Possible attack? ' . $dets['idOwner']);
        $dets['idUser'] = 0;
    }

    $domainDets = _lestatz_getDomainDets($dets['domainToken']);

    # @todo register this
    if (!$domainDets) {
        return '';
    }
     
    # Get the details about the location
    if ($dets['loc'] === 'null') {
        grace_debug('I have no location information, lets get it.');
        $location = loc_get(!isset($dets['fakeIp']) ? false : $dets['fakeIp']);
    } else {
        grace_debug('Got location information: ' . $dets['loc']);
        $location = (array) json_decode($dets['loc']);
        # New IP?
        if ($_SERVER['REMOTE_ADDR'] != $location['ipAddress']) {
            grace_debug('New ip address');
            $location = loc_get(!isset($dets['fakeIp']) ? false : $dets['fakeIp']);
        }
    }

    // Store the details
    // @todo Improve the os detection
    db_exec(sprintf(
        "INSERT INTO `lestatz_visit_log` (
			 `idOwner`,`domainId`,`timestamp`,`ip`,
			 `browser`,`browserDets`,`browserLang`,`browserVersion`,`browserLangBase`,
			 `os`,`url`,`args`,`pageName`,`pageLang`,`referrer`,
			 `country`,`region`,`city`,`post`,`coordinates`,
			 `ref`,`goal`,`visitorCode`,`lastTimeCheck`,`serverIp`,`extras`,`pageType`)
			 values ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',
				 '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
        $dets['idOwner'],
        $domainDets['idDomain'],
        time(),
        db_escape(_lestatz_anonIp($location['ipAddress'])),
        db_escape($dets['browserInfo']->browser->name),
        db_escape($dets['agent']),
        db_escape($dets['browserLang']),
        db_escape($dets['browserInfo']->browser->version),
        substr($dets['browserLang'], 0, 2),
        db_escape($dets['browserInfo']->os->name),
        db_escape($dets['domain']),
        db_escape($dets['search']),
        db_escape($dets['title']),
        db_escape($dets['lang']),
        db_escape($dets['referer']),
        db_escape($location['country']),
        $location['region'],
        $location['city'],
        $location['post'],
        $location['longitude'] . "," . $location['latitude'],
        $dets['ref'],
        $dets['goal'],
        $dets['visitorCode'],
        time(),
        $_SERVER['SERVER_ADDR'],
        db_escape($dets['extras']),
        db_escape($dets['pageType'])
             ));

    # Goals and refs can be done afterwards, if they fail they can be recovered from the main log
    _lestatz_storeGoal(
        $dets['goal'],
        $domainDets['idDomain']
     );

    _lestatz_storeRef(
        $dets['ref'],
        $domainDets['idDomain']
     );

    return json_encode($location);
}

/**@}*/
/** @}*/
