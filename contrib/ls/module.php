<?php
/** @file ls/module.php
 * LeStatz Web Manager
 */

/** \addtogroup Contrib
 *  @{
 */

/**
 * \defgroup ls
 * @{
 */

global $lettersSafe;

# A set of letters and symbols.
$lettersSafe = [
    'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
    'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','y','Z',
    '1','2','3','4','5','6','7','8','9','0',
];

//!< Access to see ALL logs in a site
define('LS_ACCESS_VIEW', 'LS_ACCESS_VIEW');

//!< Access to see my logs in a site
define('LS_ACCESS_VIEW_MINE', 'LS_ACCESS_VIEW_MINE');

/**
 * Boot up procedure
 */
function ls_bootMeUp()
{
    # Booting up routines that you require
    grace_debug('ls is in the house');
    locale_load('ls');
}

/**
 * Init function/hook.
 */
function ls_init()
{
    return [
        [
            'r' => 'ls_home',
            'action' => 'ls_home',
            'access' => 'users_loggedIn',
            'replyType' => CALA_REPLY_SKIN,
            'file' => 'domains.php',
            'params' => [
                //['key' => '', 'def' => '', 'req' => true]
            ]
        ],
        [
            'r' => 'ls_domain_new',
            'action' => 'ls_domainNew',
            'access' => 'users_loggedIn',
            'replyType' => CALA_REPLY_SKIN,
            'file' => 'domainsEdit.php',
            'params' => [
                ['key' => 'idDomain', 'def' => '', 'req' => false]
            ]
        ],
        [
            'r' => 'ls_live_stats',
            'action' => 'ls_liveStats',
            'access' => 'users_loggedIn',
            'replyType' => CALA_REPLY_SKIN,
            'file' => 'liveStats.php',
            'params' => [
                ['key' => 'idDomain', 'def' => '', 'req' => false],
                ['key' => 'live', 'def' => '1', 'req' => false]
            ]
        ],
        [
            'r' => 'ls_logs_get',
            'action' => 'ls_logsGet',
            'access' => 'users_loggedIn',
            'params' => [
                ['key' => 'last', 'req' => true],
                ['key' => 'idDomain', 'req' => true],
                ['key' => 'type', 'def' => 'live', 'req' => false],
                ['key' => 'limit', 'def' => '', 'req' => false]
            ],
            'file' => 'retrieve.php'
        ],
    ];
}

/**
 * Main page
 *
 * Externall call
 */
function ls_home()
{
    global $user;

    grace_debug("Getting a list of sites for a user");

    if ($user['idUser'] != 0) {
        //tools_goto('?w=users_login_page');
    }

    $sites = _ls_sitesGet(['idUser' => $user['idUser']]);

    return skin_this(['domains' => $sites], dirname(__FILE__) . '/skins/frontPage', 'ls');
}

/**
 * Check permissions to access a site
 */
function _ls_access(
    $user,
    $domain,
    $access = LS_ACCESS_VIEW
) {
    if ($access == LS_ACCESS_VIEW) {
        if ($domain['idUser'] == $user['idUser']) {
            return true;
        }
        return false;
    }
}

/**@}*/
/** @}*/
