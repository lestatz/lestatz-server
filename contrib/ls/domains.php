<?php

/**
 * List of websites
 */
function _ls_sitesGet(
    $filters
) {
    global $user;

    grace_debug("Getting a list of sites for a user");

    $f = [];
    if (isset($filters['idUser'])) {
        $f[] = " idUser = {$filters['idUser']}";
    }

    $where = '';
    if (count($f) > 0) {
        $where = ' WHERE ' . join(' AND ', $f);
    }

    # Unique visitors
    $q = "SELECT *
		FROM `lestatz_domains`
		$where
		ORDER BY idDomain DESC";

    $sites = db_q($q);

    if (_db_queryGood($sites)) {
        return $sites;
    }
    return false;
}

/**
 * Load a domain
 */
function _ls_domainLoad(
    $idDomain,
    $what = 'idDomain'
) {
    $q = "SELECT *
		FROM `lestatz_domains`
		WHERE $what = '$idDomain'";

    $domain = db_querySingle($q);

    if (!_db_queryGood($domain)) {
        return false;
    }

    /*
    $domain['token'] = lestatz_tokenDomainGen(
        $user,
        $domain['domain']
    );
     */
    return $domain;
}

/**
 * Get websites for a user.
 *
 * External call.
 */
function ___ls_sitesGet()
{
    global $user;

    grace_debug("Getting a list of sites for a user");

    $sites = _ls_sitesGet(['idUser' => $user['idUser']]);

    return skin_this([], dirname(__FILE__) . '/skins/sitesMineList', 'ls');
}


/**
 * Get domain information, this is only for MY domains.
 *
 * External call.
 */
function lestatz_domainGetDetails($idDomain)
{
    global $user;

    grace_debug('Getting domain informationa');

    $domain = lestatz_domainLoad($idDomain, $user);

    if ($domain['idUser'] != $user['idUser']) {
        return tools_errSet(
            'The domain does not belong to this user',
            'ERR_ERR'
         );
    }

    return $domain;
}
