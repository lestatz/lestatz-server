var Ls = {

	// The last log recieved
	lastLog: 0,

	// The domain we are working on at the moment
	idDomain: '',

	// The stats server
	server: '',

	// Current total log
	logCurrent: [],

	// Last log time
	logLastTime: 0,

	// Unique visitors in last check (live)
	liveVisitorsUniqueLast: 0,

	// Total page views in last check (live)
	livePageViewsLast: 0,

	symbolUp: '<span class="icon-circle-up2"></span>',
	symbolDown: '<span class="icon-circle-down2"></span>',
	symbolSame: '<span class="icon-pause5"></span>',

	// Timers and requests
	timerLive: true,
	timerNextRequest: false,
	timerInterval: 5000,

	// Update interval for charts in 'requests' each 10,20,30 of them
	chartsUpdateInterval: 10,

	// Number of requests done DO NOT TOUCH
	chartsRequests: 0,

	//Boot the live visits page
	bootLive(idDomain = false){

		Ls.idDomain = idDomain ? idDomain : Ls.idDomain;

		Cala.say('Get live stats for: ' + Ls.idDomain);

		// Start getting some stats
		Ls.requestLogLive();

	},

	/***********************************************************************************************
	 * Control stuff
	 */

	// Stop the timers
	timerStopAll(){
		Ls.timerLive = false;
		$('#ls_buttonStop').hide();
	},

	/**
	 * Generate a list of timezones
	 */
	timezonesSelectGen(where = 'ls_domain_edit_timezone', current = 89){

		var select = '';

		for(var i in timezones){
			select += `<option value="${timezones[i][0]}">${timezones[i][1]}</option>`;
		}
		$('#'+where).html(select);
		$('#'+where).val(current);
	},

	domainCodeSelectAll(){
		$('#lestatz_domainCode').focus();
		$('#lestatz_domainCode').select();
	},

	/***********************************************************************************************
	 * Requests
	 */

	/**
	 *	Request statz
	 *	This is the main function to do this requests in 'Live Mode'
	 */
	requestLogLive() {

		if(!Ls.timerLive){
			return false;
		}

		Cala.say('Requesting visit log for: ' + Ls.idDomain);

		$('#ls_buttonRefresh').prop('disabled', true);
		$('#ls_buttonStop').prop('disabled', true);
		$('#loader_lsLive').show();

		Ls.requestLog({
			idDomain: Ls.idDomain,
			type: 'live',
			limit: Ls.lastLog === 0 ? 10 : 0,
			success: function(log){

				// Whether new results come or not, I should report how things are going
				Ls.presentLiveCurrent(log);

				// Got some results, lets update some other stuff
				if(log.log !== false){

					Cala.info('There is a new log, lets put it on the screen');

					// Counts as an actual request
					Ls.chartsRequests++;

					//Visitors log list
					Ls.presentVisitsList(log.log)

					// A lot of information
					Ls.presentReportsGlobal(Ls.logCurrent);
				}

				// Set the next call
				if(Ls.timerLive){	Ls.timerNextRequest = setTimeout(function(){ Ls.requestLogLive() }, Ls.timerInterval) }

				$('#ls_buttonRefresh').prop('disabled', false);
				$('#ls_buttonStop').prop('disabled', false);
				$('#loader_lsLive').hide();

			},
			err: function(){
				$('#ls_buttonRefresh').prop('disabled', false);
				$('#ls_buttonStop').prop('disabled', false);
				$('#loader_lsLive').hide();
			},
		});

	},

	// Request statz via log
	requestLogPreset(){

		Cala.say('Request log via preset');

		Ls.logCurrent = [];
		$('#holder_lsVisitLog').html('');

		Cala._loadIni();

		Ls.requestLog({
			idDomain: Ls.idDomain,
			type: 'preset',
			last: $('#ls_presets').val(),
			limit: 0,
			success: function(log){

				// Whether new results come or not, I should report how things are going
				Ls.presentLiveCurrent(log);

				// Got some results, lets update some other stuff
				if(log.log !== false){

					Cala.info('There is a new log, lets put it on the screen');

					// Counts as an actual request
					Ls.chartsRequests++;

					//Visitors log list
					Ls.presentVisitsList(log.log)

					// A lot of information
					Ls.presentReportsGlobal(Ls.logCurrent);
				}

				//$('#ls_buttonRefresh').prop('disabled', false);
				//$('#ls_buttonStop').prop('disabled', false);
				//$('#loader_lsLive').hide();
				Cala._loadEnd();

			},
			err: function(){
				//$('#ls_buttonRefresh').prop('disabled', false);
				//$('#ls_buttonStop').prop('disabled', false);
				//$('#loader_lsLive').hide();
				Cala._loadEnd();
			},
		});

	},

	// Actually request logs
	requestLog(d = {}){

		Cala.toolsConnect(
			Ls.server,
			{
				'w': 'ls_logs_get',
				'last': d.last !== undefined ? d.last : Ls.lastLog,
				'idDomain': Ls.idDomain,
				'type': d.type === undefined ? 'live' : d.type,
				'limit': d.limit == undefined ? 10 : d.limit,
				'graceSkip': 1
			},
			function(r){

				Cala.say('Got some statz?');

				if(r.log.log !== false){
					Cala.info('Yes, new logs came in');

					if(r.log.length > 0){
						// Merge with the current total log
						Ls.logCurrent = Ls.tool_mergeObj(Ls.logCurrent, r.log);
						Ls.lastLog = r.log[0]['idVisit'];
					}

					// Set dates for them all
					r = Ls.tools_setDates(r);

				}else{
					r.log = false;
				}

				d.success(r);
			},
			function(e){
				Cala.err('Err');
				d.err();
			}
		);

	},

	/**********************************************************************************************
	 * Reports and anals
	 */

	/**
	 * Analyse report for a lot of stuff
	 */
	analReports(log){

		Cala.say('Analysing stats');

		var k = Object.keys(log);

		// Lenght/size of the report
		var l = k.length;

		Cala.say('A total of ' + l + ' stats where recieved');

		// Visitors
		var v = [];

		// Refs
		// Total refs
		var r = [];
		// Internal refs
		var ri = [];

		// Goals
		var g = [];

		// Pages
		var p = [];

		// Browsers
		var b = {};

		// Countries
		var c = {};

		// OS
		var o = {};

		const unique = (value, index, self) => {
			return self.indexOf(value) === index
		}

		for(var i = 0; i < l; i++){

			// Visitors
			v[i] = log[i]['visitorCode'];

			// Refs
			if(log[i]['ref'] != ''){
				r[i] = log[i]['ref'];
				if(log[i]['ref'].indexOf('int_') >= 0){
					ri[i] = log[i]['ref'];
				}
			}

			// Goals
			if(log[i]['goal'] != ''){
				g[i] = log[i]['goal'];
			}

			// Page Names
			p[i] = log[i]['pageName'];

			// Browsers
			b[log[i]['browser']] = b[log[i]['browser']] === undefined
				? 1
				: b[log[i]['browser']] + 1;

			// Countries
			c[log[i]['country']] = c[log[i]['country']] === undefined
				? 1
				: c[log[i]['country']] + 1;

			// OS
			o[log[i]['os']] = o[log[i]['os']] === undefined
				? 1
				: o[log[i]['os']] + 1;

		}

		// Unique visitors
		var uniqueV = v.filter(unique);

		// Unique refs
		var uniqueR = r.filter(unique);

		// Unique goals
		var uniqueG = g.filter(unique);

		// Last visit
		var lastTime = Cala.toolsDateParse(log[l - 1].timestamp, 'full');

		// Unique pages
		var uniqueP = p.filter(unique);

		// Unique browsers
		var browserList = Object.keys(b);

		// Unique countries
		var countriesList = Object.keys(c);

		// Unique OSs
		var osList = Object.keys(o);

		return {

			// Last visit time
			'lastTime': lastTime,

			// Total unique visitors
			'visitorsUniqueTotal': uniqueV.length,
			'visitorsUniqueTotalP': (uniqueV.length/l)*100,

			// Total visitors/visits
			'visitorsTotal': l,

			// List of unique visitors
			'visitorsUnique': uniqueV,

			//
			// Refs
			//
			// Total visits that have a ref
			'refsTotal': r.length,
			'refsIntTotal': ri.length,
			'refsTotalP': (r.length/l)*100,
			// Total unique visits that have a ref
			'refsUniqueTotal': uniqueR.length,
			'refsUniqueTotalP': (uniqueR.length/r.length)*100,
			// List of unique refs
			'refsUnique': uniqueR,

			//
			// Goals
			//
			// Total visits that have a ref
			'goalsTotal': g.length,
			'goalsTotalP': (g.length/l)*100,
			// Total unique visits that have a ref
			'goalsUniqueTotal': uniqueG.length,
			'goalsUniqueTotalP': (uniqueG.length/g.length)*100,
			// List of unique refs
			'goalsUnique': uniqueG,

			// Pages
			'pagesUnique': uniqueP.length,

			// Browsers
			'browsers': b,
			'browsersList': browserList,
			'browsersUniqueTotal': browserList.length,

			// Countries
			'countries': c,
			'countriesList': countriesList,
			'countriesUniqueTotal': countriesList.length,

			// OS
			'os': o,
			'osList': osList,
			'osUniqueTotal': osList.length,

		};

	},

	/**
	 * Analyse the live current visitors
	 * I use the global log and extract the last minute
	 */
	analLiveCurrent(){

		Cala.say('Analysing stats for new log, last time: ' + Ls.logLastTime);

		// I need only the last minute or so
		var now = Ls.logLastTime == 0 ? 0 : Ls.logLastTime - 60;

		var k = Object.keys(Ls.logCurrent);

		// Length/size of the report
		var l = k.length;

		// Visitors
		var v = [];

		// Filter unique
		const unique = (value, index, self) => {
			return self.indexOf(value) === index
		}

		for(var i = 0; i < l; i++){
			if(Ls.logCurrent[i]['timestamp'] > now){
				v[i] = Ls.logCurrent[i]['visitorCode'];
			}else{
				break;
			}
		}

		k = Object.keys(v);
		// Lenght/size of the report
		l = k.length;

		// Unique Visitors
		var uniqueV = v.filter(unique);

		// Uniques are going up or down?
		var r = [];
		r['uS'] = Ls.symbolSame;
		if(Ls.liveVisitorsUniqueLast < uniqueV.length){
			r['uS'] = Ls.symbolUp;
		}else if(Ls.liveVisitorsUniqueLast > uniqueV.length){
			r['uS'] = Ls.symbolDown;
		}

		// Total is going up or down?
		r['tS'] = Ls.symbolSame;
		if(Ls.livePageViewsLast < l){
			r['tS'] = Ls.symbolUp;
		}else if(Ls.livePageViewsLast > l){
			r['tS'] = Ls.symbolDown;
		}

		Ls.liveVisitorsUniqueLast = uniqueV.length;
		Ls.livePageViewsLast = l;

		Ls.logLastTime = Math.round(Date.now()/1000);

		return {
			'uniqueTotal': uniqueV.length,
			'uniqueSymbol': r['uS'],
			'total': l,
			'totalSymbol': r['tS']
		};

	},

	/**********************************************************************************************
	 * Visual presentations
	 */

	/**
	 * Present a quick report of the live stats
	 *  - Total page views
	 *  - Unique visitors
	 */
	presentLiveCurrent(log){

		Cala.say('Checking live stats');

		// Analyse the log
		var anal = Ls.analLiveCurrent();

		// Big info at the top
		$('#ls_currentTotalData').html(anal.total);
		$('#ls_currentTotalSymbol').html(anal.totalSymbol);
		$('#ls_currentUniqueTotalData').html(anal.uniqueTotal);
		$('#ls_currentUniqueTotalSymbol').html(anal.uniqueSymbol);

		// No need to update the top chart if nothing found
		if(log.log !== false){

			Cala.say('There is a log, I should update a few things');

			// Live stats
			var liveStatz = [[],[]];

			// Top visitors chart (line)
			// Should I start the chart or update?
			if(!Ls.chartsLiveChart){
				Cala.info('Start the top chart');
				liveStatz[0] = [0, anal.uniqueTotal];
				liveStatz[1] = [0, anal.total];
				liveStatz[2] = [Ls.tools_getTime(), Ls.tools_getTime()];
			}else{
				// Once the chart is booted If no stats are found lets just get out
				if(anal.total == 0){
					return false;
				}
			}

			if(Ls.chartsRefreshLive()){
				if(!Ls.chartsLiveChart){
					Ls.chartsLiveVisitorStart(liveStatz);
				}else{
					Ls.chartsLiveVisitorUpdate(
						Ls.tools_getTime(),
						[
							anal.uniqueTotal,
							anal.total
						]
					);
				}
			}
		}
	},

	/**
	 * Present the list of visits
	 */
	presentVisitsList(stats){

		Cala.say('Display a list of visits');

		// Goal
		var tplGoal = Cala.tplFromDiv('tpl_lsGoal');
		// Ad some stuff
		for(var i = 0; i < stats.length; i++){

			// Goal
			stats[i]['goalFlag'] = stats[i]['goal'] != '' ? Cala.tpl(tplGoal, stats[i]) : ''

			// Colour
			// @todo You should be able to configure this
			stats[i]['bStyle'] = 'color: #fff; background: #1fd6cd;';
			if(stats[i]['pageType'] != 'reg'){ stats[i]['bStyle'] = 'color: #fff; background: #a5c6e3;' }

			// Internal?
			stats[i]['tStyle'] = '';
			if(stats[i].ref.indexOf('int_') >= 0){ stats[i]['tStyle'] = 'text-decoration: underline;' }

			// Coordinates are stored the other way arround than needed for the maps
			var coordinates = stats[i].coordinates.split(',');
			stats[i].coordinates = coordinates[1] + ',' + coordinates[0];

		}

		// Set the last log recieved
		Ls.lastLog = stats[0]['idVisit'];
		var tpl = Cala.tplFromDiv('tpl_lsLiveVisit');
		var allVisits = Cala.tplMulti(tpl, stats);
		$('#holder_lsVisitLog').prepend(allVisits);

	},

	/**
	 * Present reports for a LOT of things.
	 */
	presentReportsGlobal(r){

		Cala.say('Generate a report on a log');

		// Analyse a lot of stuff from the report
		var anal = Ls.analReports(r);

		$('#visitorsUniqueTotal').html(anal.visitorsUniqueTotal);
		$('#visitorsUniqueTotalP').html(Math.round(anal.visitorsUniqueTotalP*100)/100);
		$('#visitorsUniquePerc').css('width', Math.round(Math.round(anal.visitorsUniqueTotalP*100)/100) + '%');

		$('#visitorsTotal').html(anal.visitorsTotal);

		// Last visit time
		$('#visitLast').html(anal.lastTime);

		// Refs
		$('#refsUniqueTotal').html(anal.refsUniqueTotal);

		$('#refsTotal').html(anal.refsTotal);
		$('#refsIntTotal').html(anal.refsIntTotal);
		$('#refsExtTotal').html(parseInt(anal.refsTotal) - parseInt(anal.refsIntTotal));

		$('#refsTotalP').html(Math.round(anal.refsTotalP*100)/100);
		$('#refsUniqueTotalP').html(Math.round(anal.refsUniqueTotalP*100)/100);
		$('#refsPerc').css('width', Math.round(Math.round(anal.refsTotalP*100)/100) + '%');
		$('#refsUniquePerc').css('width', Math.round(Math.round(anal.refsUniqueTotalP*100)/100) + '%');

		// Goals
		$('#goalsUniqueTotal').html(anal.goalsUniqueTotal);
		$('#goalsTotal').html(anal.goalsTotal);
		$('#goalsTotalP').html(Math.round(anal.goalsTotalP*100)/100);
		$('#goalsUniqueTotalP').html(Math.round(anal.goalsUniqueTotalP*100)/100);
		$('#goalsPerc').css('width', Math.round(Math.round(anal.goalsTotalP*100)/100) + '%');
		$('#goalsUniquePerc').css('width', Math.round(Math.round(anal.goalsUniqueTotalP*100)/100) + '%');

		// Pages
		$('#pagesUnique').html(anal.pagesUnique);

		// Browsers
		$('#browsersUniqueTotal').html(anal.browsersUniqueTotal);

		var browsersNames = anal.browsersList;
		var browsersNumbers = Object.values(anal.browsers);

		if(Ls.chartsRefreshLive()){
			Ls.chartsBrowsers(browsersNames, browsersNumbers);
		}

		// Countries
		$('#countriesUniqueTotal').html(anal.countriesUniqueTotal);
		var countriesNames = anal.countriesList;
		var countriesNumbers = Object.values(anal.countries);

		if(Ls.chartsRefreshLive()){
			Ls.chartsCountries(countriesNames, countriesNumbers);
		}

		// OS
		$('#osUniqueTotal').html(anal.osUniqueTotal);
		var osNames = anal.osList;
		var osNumbers = Object.values(anal.os);

		if(Ls.chartsRefreshLive()){
			Ls.chartsOs(osNames, osNumbers);
		}

	},

	/**********************************************************************************************
	 * Some tools
	 */

	/**
	 * Merge two objects and avoid making a reference from one to the other.
	 */
	tool_mergeObj(o, n){

		// New is empty?
		if(n.length == 0){
			return o;
		}

		// Old is empty?
		if(o.length == 0){
			return JSON.parse(JSON.stringify(n));
		}

		var nn = JSON.stringify(n);
		nn = nn.slice(0, -1);

		var no = JSON.stringify(o);
		no = no.substring(1);

		return JSON.parse(nn + ',' + no);
	},

	/**
	 * Set dates for a log
	 */
	tools_setDates(r){

		for(var i in r.log){
			r.log[i]['dateTime'] = Cala.toolsDateParse(r.log[i].timestamp, 'full');
			r.log[i]['visitTime'] = Cala.toolsDateParse(r.log[i].timestamp, 'localeTime');
		}

		return r;

	},

	/**
	 * Helper function to get h-m-s
	 */
	tools_getTime(){
		var d = new Date();
		var h = Ls.tools_addZero(d.getHours());
		var m = Ls.tools_addZero(d.getMinutes());
		var s = Ls.tools_addZero(d.getSeconds());
		return `${h}-${m}:${s}`;
	},

	/**
	 * Thank you javascript, I need to manually add zeros to seconds.
	 */
	tools_addZero(i) { if (i < 10) { i = "0" + i } return i; },

	/**********************************************************************************************
	 * Charts
	 */

	/**
	 * Create liveVisitors chart (lines)
	 */
	chartsLiveChart: false,
	chartsLiveVisitorStart(data){

		var chartLiveCanva = document.getElementById('ls_chartLiveCurrent').getContext('2d');

		Ls.chartsLiveChart = new Chart(chartLiveCanva, {
			type: 'line',
			data: {
				labels: data[2],
				datasets: [{
					label: 'Unique visitors',
					data: data[0],
					backgroundColor: [
						'rgba(59, 251, 144, 0.4)',
					],
					borderColor: [
						'rgba(59, 251, 144, 0.4)',
					],
					borderWidth: 1
				},
					{
						label: 'Total visits',
						data: data[1],
						backgroundColor: [
							'rgba(0, 176, 255, 0.4)',
						],
						borderColor: [
							'rgba(0, 176, 255, 0.4)',
						],
						borderWidth: 1
					}
				]
			},
			options: {
				lineTension: 0,
				responsive: true,
				showLine: true,
				backgroundColor: 'rgba(255, 99, 132, 0.2)',
				aspectRatio: 5
			}
		});

	},

	/**
	 * Update liveVisitors chart
	 */
	chartsLiveVisitorUpdate(label, data){

		Ls.chartsLiveChart.data.labels.push(label);
		var i = 0;
		Ls.chartsLiveChart.data.datasets.forEach((dataset) => {
			dataset.data.push(data[i]);
			i++;
		});
		Ls.chartsLiveChart.update();
	},

	/**
	 * Create browsers chart
	 */
	chartsBrowsers(labels, data){

		$('#_ls_chartBrowsersCanva').html('<canvas id="ls_chartBrowsersCanva"></canvas>');

		var chartBrowsersCanva =
			document.getElementById('ls_chartBrowsersCanva').getContext('2d');

		// Colours
		var bg = [];
		for(var i = 0; i < labels.length; i++){
			bg[i] = '#'+Math.floor(Math.random()*16777215).toString(16);
		}

		var chartsBrowser = new Chart(chartBrowsersCanva, {
			type: 'pie',
			data: {
				datasets: [{
					data: data,
					backgroundColor: bg,
					label: 'Browsers'
				}],
				labels: labels
			},
			options: {
				responsive: true,
				showLine: true,
				backgroundColor: 'rgba(255, 99, 132, 0.2)',
				legend: {
					display: true,
					position: 'left'
				}
			}
		});

	},

	/**
	 * Create countries chart
	 */
	chartsCountries(labels, data){

		$('#_ls_chartCountriesCanva').html('<canvas id="ls_chartCountriesCanva"></canvas>');

		var chartBrowsersCanva =
			document.getElementById('ls_chartCountriesCanva').getContext('2d');

		// Colours
		var bg = [];
		for(var i = 0; i < labels.length; i++){
			bg[i] = '#'+Math.floor(Math.random()*16777215).toString(16);
		}

		var chartsBrowser = new Chart(chartBrowsersCanva, {
			type: 'pie',
			data: {
				datasets: [{
					data: data,
					backgroundColor: bg,
					label: 'Countries'
				}],
				labels: labels
			},
			options: {
				responsive: true,
				legend: {
					display: false,
					position: 'left'
				}
			}
		});

	},

	/**
	 * Create os chart
	 */
	chartsOs(labels, data){

		$('#_ls_chartOsCanva').html('<canvas id="ls_chartOsCanva"></canvas>');

		var chartOsCanva =
			document.getElementById('ls_chartOsCanva').getContext('2d');

		// Colours
		var bg = [];
		for(var i = 0; i < labels.length; i++){
			bg[i] = '#'+Math.floor(Math.random()*16777215).toString(16);
		}

		var chartsBrowser = new Chart(chartOsCanva, {
			type: 'pie',
			data: {
				datasets: [{
					data: data,
					backgroundColor: bg,
					label: 'Operating Systems'
				}],
				labels: labels
			},
			options: {
				responsive: true,
				legend: {
					display: true,
					position: 'left'
				}
			}
		});

	},

	// Refresh the top chart? If done every 30 seconds it can be A LOT
	chartsRefreshLive(){
		if(Ls.chartsRequests == 0) return true;
		if((Ls.chartsRequests-1)%Ls.chartsUpdateInterval == 0) return true;
		return false;
	},

	/***********************************************************************************************
	 * Visual
	 */
	visualVisitTitleSet(idVisit){
		$('#ls_visitTitle').html($('#ls_visitItemTitle_'+idVisit).html());
	},

	/**
	 * Display visit details
	 */
	visualVisitDetsView(id){
		var statz = $(`#ls_visitItemDets_${id}`).html();
		$('#overlay_ls_visitDetailsContents').html(statz);
		$('#overlay_ls_visitDetailsContainer').css('width', '100%');
	},

	/**
	 * Close the overlay to view live statz
	 */
	visualVisitLogClose(){
		$('#overlay_ls_visitDetailsContainer').css('width', '0%');
	},

}
