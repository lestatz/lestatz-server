<?php

/**
 * Create a new domain
 */
function ls_domainNew(
    $idDomain
) {
    global $user;

    grace_debug('Create a new domain');

    # Edit?
    $domain = false;
    $code = 0;
    $ls_codeTPl = '';
    if ($idDomain != '') {
        modules_loader('ls', 'domains.php', false);
        $domain = _ls_domainLoad($idDomain, 'code');

        if ($domain['idUser'] != $user['idUser']) {
            $domain = false;
            $idDomain = 0;
        } else {
            $code = $domain['code'];

            include_once('skins/code.php');
            $ls_codeTPl = sprintf(
                $ls_codeTPl,
                conf_get('serverStore', 'lestatz', ''),
                $domain['code']
                );
        }
    } else {
        $idDomain = 0;
    }

    $form = _ls_form_domain_get($domain);

    return skin_this(
        [
             'form'     => $form,
             'idDomain' => $idDomain,
             'code'     => $code,
             'lsCode'   => $ls_codeTPl
         ],
        dirname(__FILE__) . '/skins/domainEdit',
        'ls'
     );
}

/**
 * Domains form
 */
function _ls_form_domain_get(
    $values = false
) {
    global $locale, $user;

    $form = array(
        'form' => array(
            'method' => 'POST',
            'action' => 'ls_domain_new',
            'id' => 'ls_domain_edit',
            'idColumn' => 'idDomain',
            'table' => 'lestatz_domains',
            'class' => 'pure-form',
            'ajax' => true
        ),
        'values' => $values,
        'fields' => array(
            'domain' => array(
                'type' => 'input',
                'maxsize' => '50',
                'required' => true,
            ),
            'name' => array(
                'type' => 'input',
                'class' => 'full',
                'required' => true
            ),
            'secret' => array(
                'type' => 'input',
                'required' => true,
                'maxSize' => 18,
                'class' => 'full'
            ),
            'timezone' => array(
                'type' => 'select',
                'required' => true,
                'class' => 'full'
            ),

            'created' => array(
                'type' => 'hidden',
                'required' => false,
                'value' => time()
            ),
            'idDomain' => array(
                'type' => 'hidden',
                'required' => false,
                'ignore' => true,
                'value' => 0
            ),
            'code' => array(
                'type' => 'hidden',
                'required' => false,
                'value' => ''
            ),
            'idUser' => array(
                'type' => 'hidden',
                'required' => false,
                'value' => $user['idUser']
            ),
            'submit' => array(
                'type' => 'submit',
                'class' => 'pure-button success block_on_send',
                'value' =>  $locale['ls']['newDomainButton']
            )
        )
    );

    # Edit form
    if ($values) {
        //$form['form']['action'] = 'manager_edit_business';
    }

    # Parse and skin the form
    $form = forms_get($form, dirname(__FILE__) . '/skins/domainsEditForm', 'ls');

    return $form;
}

/**
 * Check the form.
 */
function ls_domain_edit_check(
    $form
) {
    global $lettersSafe, $user;

    grace_debug('Checking bussiness form with my function');

    if ($form['fields']['idUser']['value'] != $user['idUser']) {
        $form['errors'] = 'wrongUser';
    }

    if ($form['submitted'] == FORM_SUBMITTED_GOOD) {
        if ($form['status'] == FORM_SUBMITTED_NEW) {
            $form['fields']['code']['value'] = time() . tools_secretGenerate(16, $lettersSafe);
        } else {
            # Code shall not be touched
            unset($form['fields']['code']);
        }
        return $form;
    }

    return $form;
}
