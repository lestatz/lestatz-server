/*!
 * Public stuff for LeStatz
 * v 0.5.7-stable
 * (c) 2019~ Twisted Head
 * Licence: Do no harm. You may use this sofware for free as long as it is
 * NOT to harm ANY living creature.
 *
 * <script>
 * var LeStatz_dets = {
 *		url: '',
 *		extras: JSON.stringify({key: 'value'}),
 * 	domain: 'domainToken',
 * 	uid: 'userId',
 *		visitor: '1',
 *		userToken: '',
 *		domainToken: '',
 *		idUser: '',
 *		title: '',
 *		lang: '',
 *		goal: '',
 *		ref: '',
 *		visitorCode: '',
 *		ref: '',
 * 	server: 'https://api.lestatz.e2go.org/', // Use trailing slash!!!
 *		pageType: 'int', // Set by you, the type of page
 *  };
 * LeStatz.boot(LeStatz_dets);
 * </script>
 * <script src="https://lestatz.e2go.org/public/publicLeStatz.js"></script>
 *
 */

var LeStatz = LeStatz || {};

var LeStatz = {

	url: "https://lestatz.e2go.org",

	// Browser info
	browserInfo: '',

	// Vars/Params in the url
	params: {},

	visitorGet: function(force, token){

		// Default value in order to be able to use minify
		force = force === undefined ? false : force;

		if(force === true){
			id = null;
		}else{
			var id = window.localStorage.getItem("LeStatz_id_"+token);
		}

		var id = window.localStorage.getItem('LeStatz_id_'+token);
		if(id === null
			|| id === undefined
			|| id === 'undefined'
			|| id === 'null'
			){
			// Generate id
			id = Math.round(Date.now() * (Math.random() * 1));
			window.localStorage.setItem('LeStatz_id_'+token, id);
		}
		return id;
	},

	//http://www.netlobo.com/url_query_string_javascript.html
	getParam: function(name){
		if(LeStatz.params[name] !== undefined){
			return LeStatz.params[name];
		}
		return '';
	},
	boot: function(p){
		// Browser info
		// https://github.com/faisalman/ua-parser-js
		var LeStatz_parser = new UAParser();
		LeStatz.browserInfo = JSON.stringify(LeStatz_parser.getResult());

		// Get params in the url
		document.URL.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) { LeStatz.params[key] = value });

		//Structured like this just to make it more readable
		var dets = {
			w: "lestatz_store_statz",

			domainToken: p.domainToken,

			idOwner: (p.idOwner !== undefined && p.idOwner !== false)
			? p.idOwner
			: 0,

			title: (p.title !== undefined && p.title !== false)
			? p.title
			: document.title,

			// Browser lang
			lang: navigator.language,

			referer: document.referrer,

			browserInfo: LeStatz.browserInfo,
			browserLang: navigator.language,
			agent: navigator.userAgent,
			browserV: navigator.appVersion,
			os: navigator.platform,

			goal: (p.goal !== undefined && p.goal != false)
			? p.goal
			: this.getParam('lsp_goal'),

			ref: (p.ref !== undefined && p.ref != false)
			? p.ref
			: this.getParam('lsp_ref'),

			pageType: (p.pageType !== undefined)
			? p.pageType
			: 'reg',

			visitorCode: (p.visitor !== undefined && p.visitor != false)
			? p.visitor
			: LeStatz.visitorGet(),

			extras: (p.extras !== undefined && p.extras != false)
			? JSON.stringify(p.extras)
			: JSON.stringify({}),

			fakeIp: p.fake !== undefined ? p.fake : false,

			url: window.location.href, // Full path: example.com/?hello=1 || example.com/hello/1 || exaple.com/hello/1/?x=1
			domain: window.location.host, // www.exampe.com
			ssl: window.location.protocol == 'https' ? true : false, // Secure?
			search: window.location.search // Query ?hello=1
		};

		// Do I know the location information?
		dets.loc = window.localStorage.getItem('LeStatz_loc_'+dets.domainToken);

		// Register using server...
		this.url = p.server === undefined ? this.url : p.server;
		console.log('LeStatz LeStatz, we are tracking this information: >> ' + JSON.stringify(dets) + "at: " + this.url);
		console.log("We will also track your IP and it will be anonymized to comply with GDPR.");

		var data = new FormData();
		for(var d in dets){
			data.append(d, dets[d]);
		}

		var xhttp = new XMLHttpRequest();
		//xhttp.open("POST", this.url + '?w=lestatz_store_statz');

		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				//var resp = JSON.parse(this.responseText);
				//window.localStorage.setItem('LeStatz_loc_'+dets.domainToken, resp.resp);
			}
		};

		xhttp.open("POST", this.url);
		//xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhttp.send(data);
	}
};


