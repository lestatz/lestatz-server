<?php

/**
 * Live stats page
 */
function ls_liveStats(
    $idDomain,
    $live
) {
    grace_debug('View live stats');

    modules_loader('lestatz', 'tools.php', false);

    # Get domain information
    $domain = _lestatz_getDomainDets($idDomain);

    if (!$domain) {
        tools_goto('?w=ls_main');
    }

    $tpls = file_get_contents(dirname(__FILE__) . '/skins/liveTpl.html');

    return skin_this(
         [
             'domain' => $domain,
             'live' => $live,
             'tpl' => $tpls
         ],
         dirname(__FILE__) . '/skins/liveStats',
         'ls'
     );
}
