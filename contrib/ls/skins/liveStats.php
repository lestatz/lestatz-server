
<?= $skin['tpl'] ?>

<!-- Page Title -->
<h1>
	{ls_liveStats}: <span id="ls_liveTitle"><?= $skin['domain']['name'] ?></span>
</h1>
{ls_siteCode}: <span id="ls_siteCode"><?= $skin['domain']['code'] ?></span>
<br />
{ls_lastVisit}: <span id="ls_visitLast">--</span>
<!-- //Page Title -->

<?php if (!$skin['live']): ?>
<!--
	Change dates/Presets
-->
<div id="changeDatesDiv" style="" class="calaPages rounded">

	<div>
	<span id="lestatz_loaderLogs"></span>
	<span id="lestatz_loaderLogsMsgs"></span>
	</div>
	<div id="accordion">
		<h3>
			{ls_quickLog}
		</h3>
		<div>
			<form class="pure-form" onSubmit="return false;">
				<fieldset>
					<legend>
						{ls_quickLogLeg}
					</legend>

					<select id="ls_presets">
						<option value="1H">{ls_lastHour}</option>
						<option value="6H">{ls_lastSixHours}</option>
						<option value="12H">{ls_lastTwelveHours}</option>
						<option value="today">{ls_today}</option>
						<option value="1w">{ls_thisWeek}</option>
						<option value="2w">{ls_twoWeeks}</option>
						<option value="1m">{ls_lastMoth}</option>
					</select>

					<button onClick="Ls.requestLogPreset();" class="pure-button secondary">
						{ls_logGen}
					</button>

					<button onClick="lestatz_logsDownload();" class="pure-button info">
						{ls_logDown}
					</button>

				</fieldset>
			</form>
		<!--h3>
			{ls_logLoad}
		</h3>
		<div>
			<form class="pure-form" onSubmit="return false;">
				<fieldset>
					<legend>
						{ls_logLoadLeg}
					</legend>
					<input type="file" id="files" name="logFile" multiple />
				</fieldset>
			</form>
		</div-->
	</div>

</div>
<!--
	//Change dates/Presets
-->
<?php endif; ?>

<!-- Live Stats Chart -->
<div id="ls_liveStats">

	<div class="card rounded">
		<div class="cardBody">

			<div class="pure-g">
				<div class="pure-u-1 pure-u-md-1-3 l_box">

					<!-- Total Page Views -->
					<div class="pure-g">
						<div class="pure-u-1-2 l_box">
							<h4>
								{ls_pageViewsTotal}
							</h4>
						</div>
						<div class="pure-u-1-2 l_box">
							<div id="ls_currentTotal">
								<span id="ls_currentTotalSymbol"></span>
								<span id="ls_currentTotalData"></span>
							</div>
						</div>
					</div>
					<!-- //Total Page Views -->

				</div>
				<div class="pure-u-1 pure-u-md-1-3 l_box">

					<!-- Unique Visitors -->
					<div class="pure-g">
						<div class="pure-u-1-2">
							<h4>
								{ls_visitorsUnique}
							</h4>
						</div>
						<div class="pure-u-1-2">
							<div id="ls_currentUniqueTotal">
								<span id="ls_currentUniqueTotalSymbol"></span>
								<span id="ls_currentUniqueTotalData"></span>
							</div>
						</div>
					</div>
					<!-- //Unique Visitors -->

				</div>

				<div class="pure-u-1 pure-u-md-1-3 l_box">

					<span id="ls_buttonLive">
						<button onClick='Ls.refreshMain();' class="pure-button primary huge" id="ls_buttonRefresh">
							<span class="icon-spinner112"></span>
						</button>

						<button onClick='Ls.timerStopAll();' class="pure-button warning" id="ls_buttonStop">
							<span class="icon-switch2"></span>
						</button>
					</span>

					<span id="loader_lsLive" style="display: none; padding-top: 25px;"><img src="web/lestatz/loaderLive.gif"></span>

				</div>
			</div>

			<!-- Live chart -->
			<canvas id="ls_chartLiveCurrent"></canvas>
			<!-- //Live chart -->
		</div>
	</div>
</div>
<!-- //Live Stats Chart -->

<!--
	Reports
-->

<div id="ls reportsSection">
	<div class="pure-g">
		<div class="pure-u-1 pure-u-md-1-2 l_box">
			<!-- Visitors Log (List of visits) -->
			<div id="ls_visitsLogsContainer">

				<hr class="separate" />
				<div id="ls_visitTitle">&nbsp;</div>
				<hr class="separate" />

				<!-- Visits log -->
				<div id="holder_lsVisitLog"></div>
				<!-- //Visits log -->

				<!-- Overlay with visit details -->
				<div id="overlay_ls_visitDetailsContainer">
					<div id="overlay_ls_visitLogCloseBar" onClick="Ls.visualVisitLogClose();">
						<span class="icon-arrow-left22"></span>
					</div>
					<div id="overlay_ls_visitDetailsContents"></div>
				</div>
				<!-- //Overlay with visit details -->

			</div>
			<!-- //Visitors Log (List of visits) -->

		</div>
		<div class="pure-u-1 pure-u-md-1-2 l_box">

			<!-- Brief report -->
			<div class="card rounded">
				<div class="cardBody">

					<!-- Total visitors quick view -->
					<table width="100%">
						<tr>
							<th rowspan="2" width="10%">
								<h2>
									{ls_pageViews}
								</h2>

							</th>
							<th align="center">
								{ls_pageViewsT}
							</th>
							<th>
								{ls_visitorsUnique}
							</th>
						</tr>
						<tr>
							<td valign="top" width="50%" align="center">
								<chip class="info" id="visitorsTotal">--</chip>
							</td>
							<td>
								<span id="visitorsUniqueTotal">--</span> (<span id="visitorsUniqueTotalP"></span> %)
								<div class="secondary" style="margin: 0px; height: 5px;" id="visitorsUniquePerc"></div>
							</td>
						</tr>
					</table>
					<!-- //Total visitors quick view -->

					<hr class="separate" />

					<!-- Visitors via Ref -->
					<table width="100%">
						<tr>
							<th rowspan="2" width="10%">
								<h2>
									{ls_refs}
								</h2>

							</th>
							<th align="center">
								{ls_total}
							</th>
							<th align="center">
								{ls_totalP}
							</th>
							<th align="center">
								{ls_unique}
							</th>

						</tr>
						<tr>
							<td valign="top" width="50%" align="center">
								<chip class="info" id="refsTotal"></chip>
								(i) <chip class="info" id="refsIntTotal"></chip>
								(e) <chip class="info" id="refsExtTotal"></chip>
							</td>
							<td>
								<chip class="success" id="refsTotalP"></chip> %
								<div class="secondary" style="margin: 0px; height: 5px;" id="refsPerc"></div>
							</td>
							<td>
								<span id="refsUniqueTotal"></span> / <span id="refsUniqueTotalP"></span> %
								<div class="secondary" style="margin: 0px; height: 5px;" id="goalsUniquePerc"></div>

							</td>
						</tr>
					</table>
					<!-- //Visitors via Ref -->

					<hr class="separate" />

					<!-- Visitors via Goal -->
					<table width="100%">
						<tr>
							<th rowspan="2" width="10%">
								<h2>
									{ls_goals}
								</h2>
							</th>
							<th align="center">
								{ls_total}
							</th>
							<th align="center">
								{ls_totalP}
							</th>
							<th align="center">
								{ls_unique}
							</th>

						</tr>
						<tr>
							<td valign="top" width="50%" align="center">
								<chip class="success" id="goalsTotal"></chip>
							</td>
							<td>
								<chip class="secondary" id="goalsTotalP"></chip> %
								<div class="secondary" style="margin: 0px; height: 5px;" id="goalsPerc"></div>
							</td>
							<td>
								<span id="goalsUniqueTotal"></span> / <span id="goalsUniqueTotalP"></span>
								<div class="secondary" style="margin: 0px; height: 5px;" id="refsUniquePerc"></div>
							</td>
						</tr>
					</table>
					<!-- //Visitors via Goal -->

					<hr class="separate" />

					<!-- Other Details -->
					<table width="95%">
						<tr>
							<td>
								{ls_pagesUnique}
							</td>
							<td>
								{ls_browsers}
							</td>
							<td>
								{ls_countries}
							</td>
							<td>
								{ls_oss}
							</td>
						</tr>
						<tr>
							<td width="25%">
								<span id="pagesUnique" class="pure-button success fullish"></span>
							</td>
							<td width="25%">
								<span id="browsersUniqueTotal" class="pure-button success fullish"></span>
							</td>
							<td width="25%">
								<span id="countriesUniqueTotal" class="pure-button success fullish"></span>
							</td>
							<td width="25%">
								<span id="osUniqueTotal" class="pure-button success fullish"></span>
							</td>
						</tr>
					</table>
					<!-- //Other Details -->
				</div>
			</div>
			<!-- //Brief report -->

			<!-- Charts	-->
			<div id="chartsContainer">

				<div class="pure-g">
					<div class="pure-u-1 pure-u-md-1-3">
						<!-- Browsers Chart -->
						<div class="card rounded">
							<div class="cardHeader rounded">
								<h2 class="ls_chartTitles">
									{ls_browsers}
								</h2>
							</div>
							<div class="cardBody">
								<div id="_ls_chartBrowsersCanva">
								</div>
							</div>
						</div>
					</div>
					<!-- //Browsers Chart -->
					<div class="pure-u-1 pure-u-md-1-3">
						<!-- Countries Chart -->
						<div class="card rounded">
							<div class="cardHeader rounded">
								<h2 class="ls_chartTitles">
									{ls_countries}
								</h2>
							</div>
							<div class="cardBody">
								<div id="_ls_chartCountriesCanva">
									<!-- Hero goes the canva -->
								</div>
							</div>
						</div>
						<!-- //Countries Chart -->
					</div>

					<div class="pure-u-1 pure-u-md-1-3">
						<!-- OS Chart -->
						<div class="card rounded">
							<div class="cardHeader rounded">
								<h2 class="ls_chartTitles">
									{ls_oss}
								</h2>
							</div>
							<div class="cardBody">
								<div id="_ls_chartOsCanva">
									<!-- Hero goes the canva -->
								</div>
							</div>
						</div>
						<!-- //OS Chart -->
					</div>
				</div>
			</div>
			<!-- // Charts	-->

		</div>
	</div>
</div>
<!--
	Reports
-->


<script>

	// Boot the live stats
	$(document).ready(function(){
		Ls.idDomain = '<?= $skin['domain']['code'] ?>';
		<?php if ($skin['live']): ?>
			Ls.bootLive('<?= $skin['domain']['code'] ?>');
		<?php else: ?>
			$('#ls_liveStats').slideUp();
		<?php endif; ?>
	});

</script>
