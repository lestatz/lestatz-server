<h1>
	Domain Manager
</h1>

<?php print $skin['form'] ?>

<input type="hidden" id="_redirect" value="ls_home">

<?php if ($skin['idDomain'] != 0): ?>

<div id="ls_domainCodes">

	<div class="card rounded">
		<div class="cardHeader">
			{lestatz_domainCode}
		</div>
		<div class="cardBody">

			<form class="pure-form pure-form-stacked" onSubmit="return false;">
				<fieldset>

					<label for="lestatz_token" class="formLabel">
						{lestatz_token}
					</label>
					<input type="text" name="lestatz_code" id="lestatz_code" class="full" readonly="readonly" value="<?= $skin['code']?>"/>
					<div class="help">
						{lestatz_tokenHelp}
					</div>

					<hr class="separate" />
			<div class="help">
				{lestatz_domainCodeH}
			</div>

			<hr class="separate" />
			<textarea class="full" id="lestatz_domainCode" rows="25" onClick="Ls.domainCodeSelectAll();" readonly="readonly"><?= $skin['lsCode'] ?></textarea>
				</fieldset>
			</form>
		</div>
	</div>

</div>
<?php endif; ?>

