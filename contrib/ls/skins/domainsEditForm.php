
<?php print $skin['head'] ?>

<fieldset>

	<div class="cala_form_section">
		<p class="cala_formTitle">Domain's Url</p>
		<?php print $skin['domain'] ?>
	</div>	

	<div class="cala_form_section">
		<p class="cala_formTitle">Domain Name</p>
		<?php print $skin['name'] ?>
	</div>

	<div class="cala_form_section">
		<p class="cala_formTitle">Secret</p>
		<?php print $skin['secret'] ?>	

		<p class="cala_formTitle">TimeZone</p>
		<?php print $skin['timezone'] ?>	

	</div>

	<div style="margin-top: 10px;" align="right">
		<?php print $skin['submit'] ?>
	</div>
		
	<?= $skin['idDomain'] ?>
	<?= $skin['_formStatus'] ?>

</fieldset>
</form>

<script>
$(document).ready(function(){
	Ls.timezonesSelectGen();
})
</script>


