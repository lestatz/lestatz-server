<h1>LeStatz</h1>

<a href="?w=ls_domain_new" class="pure-button full success broad">
	<span class="icon-plus"></span> {ls_newDomainCreate}
</a>

<hr class="separate" />

<!-- Domains -->
<?php if ($skin['domains']): foreach ($skin['domains'] as $d): ?>
<div class="card rounded">
	<div class="cardBody rounded">
		<h4 class="lestatz_domainName">
			<?= $d['name'] ?>
		</h4>

		<hr class="separate" />
		
		<a href="?w=ls_live_stats&idDomain=<?= $d['code'] ?>&live=1" class="pure-button info">
			<span class=" icon-stats-dots2"></span>
		</a>

		<a href="?w=ls_live_stats&idDomain=<?= $d['code'] ?>&live=0" class="pure-button success">
			<span class=" icon-pie-chart2"></span>
		</a>

		<a href="?w=ls_domain_new&idDomain=<?= $d['code'] ?>" class="pure-button darkLight">
			<span class="icon-cog4"></span>
		</a>

		<hr class="separate" />
		<a href="<?= $d['domain'] ?>" target="_blank" class="">
			<chip class="darkLight">
			<span class=" icon-link"></span> <?= $d['domain'] ?>
			</chip>
		</a>
	</div>
</div>
<?php endforeach; endif; ?>
<!-- //Domains -->



