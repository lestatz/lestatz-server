<?php

/**
 * Get logs
 *
 * Externall call
 */
function ls_logsGet(
    $last,
    $idDomain,
    $type,
    $limit
) {
    global $user;

    grace_debug("Getting your statz => last: $last | domain: $idDomain");

    modules_loader('lestatz', 'tools.php', false);

    # Get domain information
    $domain = _lestatz_getDomainDets($idDomain);

    if (!$domain) {
        return tools_errSet('Domain does not exist', CALA_ERR);
    }

    if (!_ls_access($domain, $user)) {
        return tools_errSet('Access denied to see this log', CALA_ERR);
    }

    modules_loader('lestatz', 'tools.php', false);

    $filters = [];

    # Presets and files
    if ($type == 'preset' || $type == 'file') {
        grace_debug('Preset based request: ' . $last);
          
        # Presets
        $presets = [
            '1H' => '1 hour ago',
            '6H' => '6 hours ago',
            '12H' => '6 hours ago',
            'today' => 'yesterday midnight',
            '1w' => '1 week ago',
            '2w' => '2 weeks ago',
            '1m' => '1 month ago',
        ];

        if (!array_key_exists($last, $presets)) {
            return tools_errSet(
                'The preset does not exist',
                'ERR_ERR'
            );
        }
        /*
                $w = [
                    'timestamp > ' . strtotime($presets[$last])
                ];
         */
        $filters = [
              'idDomain' => $domain['idDomain'],
              'since'  => strtotime($presets[$last])
          ];

        $log = _lestatz_logLoad($filters);

        /*$log = _lestatz_logLoad(
                $domain['idDomain'],
                $w
        );*/

        # Download?
        if ($type == 'file') {
            params_set('replyType', 'attach');
        }
    } else {

         # Live logs
        $filters['limit'] = $limit == '0' ? '' : "LIMIT $limit";
        if ($last != 0) {
            $filters['latest'] = $last;
        }

        $filters['idDomain'] = $domain['idDomain'];

        $log = _lestatz_logLoad($filters);
    }

    //_lestatz_printResp($log);

    if (!$log) {
        grace_debug('No logs found');
        $log = [
             'time' => time(),
             'log' => false
         ];
    }

    $log = [
         'time' => time(),
         'log' => $log
     ];

    if ($type == 'file') {
        grace_debug('Download log');
        return [
             'fileName' => time() . '.csv',
             'text' => JSON_encode($log) //_lestatz_logToCSV($log)
         ];
    }

    return $log;
}

/**
 * Helper function to load logs.
 */
function _lestatz_logLoad(
    $filters
) {
    $f = [];

    if (isset($filters['latest'])) {
        $f[] = ' idVisit > ' . $filters['latest'];
    }
     
    if (isset($filters['since'])) {
        $f[] = ' timestamp > ' . $filters['since'];
    }

    $limit = '';
    if (isset($filters['limit']) && $filters['limit'] != '') {
        $limit = $filters['limit'];
    }

    if (count($f) > 0) {
        $f = ' AND ' . implode(' AND ', $f);
    } else {
        $f = '';
    }

    $q = sprintf(
        'SELECT * 
		FROM `lestatz_visit_log`  
		WHERE domainId = %s 
		%s 
		ORDER BY idVisit DESC
		%s',
        $filters['idDomain'],
        $f,
        $limit
        );

    $log = db_q($q);

    if (!_db_queryGood($log)) {
        return false;
    }

    return $log;
}
