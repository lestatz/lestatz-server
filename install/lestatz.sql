-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 20, 2020 at 10:48 AM
-- Server version: 10.1.47-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ls_baseDev`
--

-- --------------------------------------------------------

--
-- Table structure for table `cala_files`
--

CREATE TABLE `cala_files` (
  `idFile` int(10) UNSIGNED NOT NULL,
  `md5` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp` int(11) UNSIGNED NOT NULL,
  `size` int(11) UNSIGNED NOT NULL,
  `idUser` int(11) UNSIGNED NOT NULL,
  `downloadCode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fileType` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cala_logger`
--

CREATE TABLE `cala_logger` (
  `idLog` int(10) UNSIGNED NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL,
  `module` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'reg(ular), ale(rt), inf(o), err(or)',
  `idUser` int(10) UNSIGNED NOT NULL,
  `severity` int(10) UNSIGNED NOT NULL COMMENT 'From 1 to 10, 10 is high',
  `ip` varchar(50) NOT NULL,
  `additional` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Any additional details',
  `path` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `request` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cala_roles`
--

CREATE TABLE `cala_roles` (
  `idRole` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cala_rolesPerms`
--

CREATE TABLE `cala_rolesPerms` (
  `idRole` int(10) UNSIGNED NOT NULL,
  `perm` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cala_sessions`
--

CREATE TABLE `cala_sessions` (
  `idSession` int(11) UNSIGNED NOT NULL,
  `idUser` int(11) UNSIGNED NOT NULL,
  `sessionKey` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastAccess` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cala_users`
--

CREATE TABLE `cala_users` (
  `idUser` int(11) UNSIGNED NOT NULL,
  `fullName` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userName` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp` int(11) UNSIGNED NOT NULL,
  `lastAccess` int(11) UNSIGNED NOT NULL,
  `pwd` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci COMMENT 'Any and all settings you would like to set',
  `pk` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The public key',
  `private` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Private accounts, not for public viewing'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cala_usersRoles`
--

CREATE TABLE `cala_usersRoles` (
  `idUser` int(10) UNSIGNED NOT NULL,
  `idRole` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lestatz_domains`
--

CREATE TABLE `lestatz_domains` (
  `idDomain` int(10) UNSIGNED NOT NULL,
  `idUser` int(10) UNSIGNED NOT NULL,
  `domain` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` int(10) UNSIGNED NOT NULL,
  `secret` varchar(18) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timezone` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` int(10) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='A list of domains per user';

-- --------------------------------------------------------

--
-- Table structure for table `lestatz_goals`
--

CREATE TABLE `lestatz_goals` (
  `idGoal` int(10) UNSIGNED NOT NULL,
  `idUser` int(10) UNSIGNED NOT NULL,
  `goal` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` int(10) UNSIGNED NOT NULL,
  `idDomain` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='A list of goals per user';

-- --------------------------------------------------------

--
-- Table structure for table `lestatz_refs`
--

CREATE TABLE `lestatz_refs` (
  `idRef` int(10) UNSIGNED NOT NULL,
  `idUser` int(10) UNSIGNED NOT NULL,
  `ref` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` int(10) UNSIGNED NOT NULL,
  `idDomain` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='A list of refs per user';

-- --------------------------------------------------------

--
-- Table structure for table `lestatz_visit_log`
--

CREATE TABLE `lestatz_visit_log` (
  `idVisit` int(10) UNSIGNED NOT NULL,
  `idOwner` int(10) UNSIGNED NOT NULL COMMENT 'Owner of the page',
  `domainId` int(10) UNSIGNED NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL,
  `ip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `browser` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `browserDets` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `browserLang` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `browserVersion` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `browserLangBase` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The base lang e.g. en for English in en_UK',
  `os` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `args` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pageName` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pageLang` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referrer` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `region` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coordinates` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The refering page according to a user setting',
  `goal` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The goal set by the user',
  `visitorCode` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '''Unique'' identifier for the visitor (idclient,ip,browser,os,url)',
  `lastTimeCheck` int(10) UNSIGNED NOT NULL COMMENT 'Last time that we know of the person being in the page',
  `serverIP` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The IP serving the page',
  `extras` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pageType` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='The main registry of visits, this can become quite big';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cala_files`
--
ALTER TABLE `cala_files`
  ADD PRIMARY KEY (`idFile`);

--
-- Indexes for table `cala_logger`
--
ALTER TABLE `cala_logger`
  ADD PRIMARY KEY (`idLog`),
  ADD KEY `request` (`request`);

--
-- Indexes for table `cala_roles`
--
ALTER TABLE `cala_roles`
  ADD PRIMARY KEY (`idRole`) USING BTREE;

--
-- Indexes for table `cala_sessions`
--
ALTER TABLE `cala_sessions`
  ADD PRIMARY KEY (`idSession`),
  ADD KEY `sessionKey` (`sessionKey`(191));

--
-- Indexes for table `cala_users`
--
ALTER TABLE `cala_users`
  ADD PRIMARY KEY (`idUser`),
  ADD UNIQUE KEY `userName` (`userName`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `lestatz_domains`
--
ALTER TABLE `lestatz_domains`
  ADD PRIMARY KEY (`idDomain`),
  ADD UNIQUE KEY `code` (`code`) USING BTREE;

--
-- Indexes for table `lestatz_goals`
--
ALTER TABLE `lestatz_goals`
  ADD PRIMARY KEY (`idGoal`),
  ADD UNIQUE KEY `idGoal` (`idGoal`),
  ADD UNIQUE KEY `goalsPerDomain` (`idDomain`,`goal`),
  ADD KEY `goal` (`goal`),
  ADD KEY `idDomain` (`idDomain`);

--
-- Indexes for table `lestatz_refs`
--
ALTER TABLE `lestatz_refs`
  ADD PRIMARY KEY (`idRef`),
  ADD UNIQUE KEY `refsPerDomain` (`idDomain`,`ref`),
  ADD KEY `ref` (`ref`),
  ADD KEY `idDomain` (`idDomain`);

--
-- Indexes for table `lestatz_visit_log`
--
ALTER TABLE `lestatz_visit_log`
  ADD PRIMARY KEY (`idVisit`),
  ADD KEY `user_id` (`idOwner`),
  ADD KEY `ip` (`ip`),
  ADD KEY `visitorCode` (`visitorCode`),
  ADD KEY `serveIP` (`serverIP`),
  ADD KEY `zip` (`post`),
  ADD KEY `domain_id` (`domainId`),
  ADD KEY `pageName` (`pageName`(191)),
  ADD KEY `goal` (`goal`),
  ADD KEY `ref` (`ref`),
  ADD KEY `lang` (`browserLang`),
  ADD KEY `browser` (`browser`),
  ADD KEY `pageLang` (`pageLang`),
  ADD KEY `browserLangBase` (`browserLangBase`),
  ADD KEY `browserVersion` (`browserVersion`),
  ADD KEY `pageType` (`pageType`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cala_files`
--
ALTER TABLE `cala_files`
  MODIFY `idFile` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cala_logger`
--
ALTER TABLE `cala_logger`
  MODIFY `idLog` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cala_roles`
--
ALTER TABLE `cala_roles`
  MODIFY `idRole` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cala_sessions`
--
ALTER TABLE `cala_sessions`
  MODIFY `idSession` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cala_users`
--
ALTER TABLE `cala_users`
  MODIFY `idUser` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lestatz_domains`
--
ALTER TABLE `lestatz_domains`
  MODIFY `idDomain` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lestatz_goals`
--
ALTER TABLE `lestatz_goals`
  MODIFY `idGoal` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lestatz_refs`
--
ALTER TABLE `lestatz_refs`
  MODIFY `idRef` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lestatz_visit_log`
--
ALTER TABLE `lestatz_visit_log`
  MODIFY `idVisit` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
